/* ex: set ts=4: */
/***************************************************************************
*  ipod-device-test.c
*  Copyright (C) 2005 Novell 
*  Written by Aaron Bockover <aaron@aaronbock.net>
****************************************************************************/

/*  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of version 2.1 of the GNU Lesser General Public
 *  License as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include "ipod-device.h"
#include "ipod-device-event-listener.h"
#include "hal-common.h"
#include <glib.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <string.h>

static void
device_added_cb(IpodDeviceEventListener *listener, gchar *udi)
{
    IpodDevice *device;
    
    g_message("Device Added: %s", udi);
    device = ipod_device_new(udi);
    
    if(device == NULL) {
        g_printf("  Not a Valid iPod! Try Rebooting the iPod\n");
    } else {
        ipod_device_debug(device);
        g_object_unref(device);
    }
}

static void
device_removed_cb(IpodDeviceEventListener *listener, gchar *udi)
{
    g_message("Device Removed: %s", udi);
}

static void list_devices()
{
    IpodDevice *device;
    GList *devices;
    gint i, n;

    devices = ipod_device_list_devices();
    n = g_list_length(devices);
    
    if(n == 0) {
        g_printf("No iPod devices present\n");
        return;
    }

    for(i = 0; i < n; i++) {
        device = (IpodDevice *)g_list_nth_data(devices, i);
        ipod_device_debug(device);
        g_object_unref(device);
    }

    g_list_free(devices);
}

static void list_device_udis()
{
    gchar *udi;
    GList *devices;
    gint i, n;

    devices = ipod_device_list_device_udis();
    n = g_list_length(devices);
    
    if(n == 0) {
        g_printf("No iPod devices present\n");
        return;
    }

    for(i = 0; i < n; i++) {
        udi = (gchar *)g_list_nth_data(devices, i);
        g_printf("iPod UDI: %s\n", udi);
        g_free(udi);
    }

    g_list_free(devices);
}

static gint eject_device(gchar *id)
{
    IpodDevice *device = ipod_device_new(id);
    gint ret = 1;
    
    if(device == NULL) {
        g_printf("Could not get device for '%s'\n", id);
        return 1;
    }
    
    switch(ipod_device_eject(device, NULL)) {
        case EJECT_OK:
            g_printf("Ejected `%s' OK\n", id);
            ret = 0;
            break;
        case EJECT_ERROR:
            g_printf("Could not Eject `%s'\n", id);
            ret = 1;
            break;
        case EJECT_BUSY:
            g_printf("Could not Eject `%s', Device Busy\n", id);
            ret = 1;
            break;
    }
    
    g_object_unref(device);
    
    return ret;
}

static gint eject_all_devices()
{
    gchar *udi;
    GList *devices;
    gint i, n;
    gboolean failure = FALSE;

    devices = ipod_device_list_device_udis();
    n = g_list_length(devices);
    
    if(n == 0) {
        g_printf("No iPod devices present\n");
        return 1;
    }

    for(i = 0; i < n; i++) {
        udi = (gchar *)g_list_nth_data(devices, i);
        if(eject_device(udi) != 0)
            failure = TRUE;
        g_free(udi);
    }

    g_list_free(devices);
    
    return failure ? 1 : 0;
}

static int debug_image(gchar *path)
{
    IpodDevice *device = ipod_device_new(path);
    if(device == NULL) {
        g_printf("Error: Path '%s' does not contain an iPod Image\n", path);
        return 1;
    }
    
    ipod_device_debug(device);
    g_object_unref(device);
    return 0;
}

static int set_settings(gchar *id, gchar *name, gchar *user, gchar *host)
{
    IpodDevice *device = ipod_device_new(id);
    int ret = 0;
    
    if(device == NULL) {
        g_printf("Could not get device for '%s'\n", id);
        return 1;
    }
    
    if(name != NULL)
        g_object_set(device, "device-name", name, NULL);
    
    if(user != NULL)
        g_object_set(device, "user-name", user, NULL);
    
    if(host != NULL)
        g_object_set(device, "host-name", host, NULL);

    
    
    if(!(ret = ipod_device_save(device, NULL)))
        g_printf("Error: Could not save properties to iPod\n");
    
    g_object_unref(device);
    
    return !ret;
}

static void help()
{
    g_printf(
    "ipod: Test control program for the IpodDevice and IpodDeviceEventListener\n"
    "GObject classes by Aaron Bockover <aaron@aaronbock.net>\n\n"
    "Options:\n\n"
    "    --list             List and debug all connected iPods\n"
    "    --list-udis        List all iPod Volume HAL UDIs\n"
    "    --eject [<device>] Eject <device> (pass a device node, mount point\n"
    "                       or HAL UDI for <device>), or if no <device> is\n"
    "                       is passed, eject ALL iPods\n"
    "    --image <path>     Debug an iPod Image\n"
    "    --set-properties <device> <name> <user> <host>\n"
    "                       Set the name, user, and host settings on an iPod\n"
    "    --model-xml        Show the supported models table as XML\n"
    "    --help             Show this help\n\n"
    "The default action is to create an IpodDeviceEventListener object and\n"
    "respond to iPod/HAL signals\n\n");
}

int main(int argc, char **argv)
{    
    GMainLoop *loop;
    GMainContext *loop_context;
    IpodDeviceEventListener *listener;
    
    gchar *name = NULL, *user = NULL, *host = NULL;
    
    g_type_init();

    if(argc > 1 && strcmp(argv[1], "--help") == 0) {
        help();
        exit(1);
    } else if(argc > 1 && strcmp(argv[1], "--list") == 0) {
        list_devices();
        exit(0);
    } else if(argc > 1 && strcmp(argv[1], "--list-udis") == 0) {
        list_device_udis();
        exit(0);
    } else if(argc > 1 && strcmp(argv[1], "--model-xml") == 0) {
        ipod_device_dump_model_table_as_xml();
        exit(0);
    } else if(argc > 2 && strcmp(argv[1], "--eject") == 0) {
        exit(eject_device(argv[2]));
    } else if(argc > 1 && strcmp(argv[1], "--eject") == 0) {
        exit(eject_all_devices());
    } else if(argc > 2 && strcmp(argv[1], "--image") == 0) {
        exit(debug_image(argv[2]));
    } else if(argc > 3 && strcmp(argv[1], "--set-properties") == 0) {
        switch(argc) {
            case 4:
                name = argv[3];
                break;
            case 5:
                name = argv[3];
                user = argv[4];
                break;
            case 6:
            default:
                name = argv[3];
                user = argv[4];
                host = argv[5];
                break;
        }
        
        exit(set_settings(argv[2], name, user, host));
    } else {
        g_printf("Listening for iPod-specific HAL events...\n");
        loop_context = g_main_context_default();    
        loop = g_main_loop_new(loop_context, FALSE);
        ipod_device_set_global_main_context(loop_context);
        listener = ipod_device_event_listener_new();
        g_signal_connect(listener, "device-added", G_CALLBACK(device_added_cb), NULL);
        g_signal_connect(listener, "device-removed", G_CALLBACK(device_removed_cb), NULL);
        g_main_loop_run(loop);
    }
    
    exit(0);
}
