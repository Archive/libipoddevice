/* ex: set ts=4: */
/***************************************************************************
*  ipod-device-event-listener.h
*  Copyright (C) 2005 Novell 
*  Written by Aaron Bockover <aaron@aaronbock.net>
****************************************************************************/

/*  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of version 2.1 of the GNU Lesser General Public
 *  License as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */

#ifndef IPOD_DEVICE_EVENT_LISTENER_H
#define IPOD_DEVICE_EVENT_LISTENER_H

#include <glib.h>
#include <glib-object.h>

#include <libhal.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>

#include "ipod-device.h"

G_BEGIN_DECLS

#define TYPE_IPOD_DEVICE_EVENT_LISTENER         (ipod_device_event_listener_get_type ())
#define IPOD_DEVICE_EVENT_LISTENER(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), TYPE_IPOD_DEVICE_EVENT_LISTENER, IpodDeviceEventListener))
#define IPOD_DEVICE_EVENT_LISTENER_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), TYPE_IPOD_DEVICE_EVENT_LISTENER, IpodDeviceEventListenerClass))
#define IS_IPOD_DEVICE_EVENT_LISTENER(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), TYPE_IPOD_DEVICE_EVENT_LISTENER))
#define IS_IPOD_DEVICE_EVENT_LISTENER_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), TYPE_IPOD_DEVICE_EVENT_LISTENER))
#define IPOD_DEVICE_EVENT_LISTENER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), TYPE_IPOD_DEVICE_EVENT_LISTENER, IpodDeviceEventListenerClass))

enum {
    DEVICE_ADDED,
    DEVICE_REMOVED,
    NOTIFY_DEVICE_ADDED,
    NOTIFY_DEVICE_REMOVED,
    LAST_SIGNAL
};

typedef struct IpodDeviceEventListenerPrivate IpodDeviceEventListenerPrivate;

typedef struct {
    GObject parent;
    IpodDeviceEventListenerPrivate *priv;
} IpodDeviceEventListener;

typedef struct {
    GObjectClass parent_class;
    
    /* Signals */
    void (*device_added)(IpodDeviceEventListener *listener, const gchar *udi);
    void (*device_removed)(IpodDeviceEventListener *listener, const gchar *udi);
    void (*notify_device_added)(IpodDeviceEventListener *listener);
    void (*notify_device_removed)(IpodDeviceEventListener *listener);
} IpodDeviceEventListenerClass;


GType ipod_device_event_listener_get_type();
IpodDeviceEventListener *ipod_device_event_listener_new();

#endif /* IPOD_DEVICE_EVENT_LISTENER_H */
