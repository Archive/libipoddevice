#include <glib.h>

#include "ipod-device.h"

#define GB 1024

typedef struct _IpodModel {
    const gchar *model_number;
    const guint64 capacity;
    guint model_type;
    guint generation;
    const gchar *serial_numbers;
} IpodModel;

static const IpodModel ipod_model_table [] = {
    /* Handle idiots who hose their iPod file system, or 
       lucky people with iPods we don't yet know about*/
    {"Invalid", 0,    MODEL_TYPE_INVALID,    UNKNOWN_GENERATION, NULL },
    {"Unknown", 0,    MODEL_TYPE_UNKNOWN,    UNKNOWN_GENERATION, NULL },
    
    /* NOTE: Model code data for first and second generations may not
             be entirely accurate, but it's better than nothing */
    
    /* First Generation */
    {"8513", 5  * GB, MODEL_TYPE_REGULAR,    FIRST_GENERATION, NULL },
    {"8541", 5  * GB, MODEL_TYPE_REGULAR,    FIRST_GENERATION, "LG6NAM" },
    {"8541", 10 * GB, MODEL_TYPE_REGULAR,    FIRST_GENERATION, "ML1MME" },
    {"8697", 5  * GB, MODEL_TYPE_REGULAR,    FIRST_GENERATION, NULL },
    {"8709", 10 * GB, MODEL_TYPE_REGULAR,    FIRST_GENERATION, "NGE" },
    
    /* Second Generation */
    {"8737", 10 * GB, MODEL_TYPE_REGULAR,    SECOND_GENERATION, "NGE" },
    {"8740", 10 * GB, MODEL_TYPE_REGULAR,    SECOND_GENERATION, "NGE" },
    {"8738", 20 * GB, MODEL_TYPE_REGULAR,    SECOND_GENERATION, "MMCMMF" },
    {"8741", 20 * GB, MODEL_TYPE_REGULAR,    SECOND_GENERATION, "MMCMMF" },
    
    /* Third Generation */
    {"8976", 10 * GB, MODEL_TYPE_REGULAR,    THIRD_GENERATION, "NRH" },
    {"8946", 15 * GB, MODEL_TYPE_REGULAR,    THIRD_GENERATION, NULL },
    {"9460", 15 * GB, MODEL_TYPE_REGULAR,    THIRD_GENERATION, "QQF" },
    {"9244", 20 * GB, MODEL_TYPE_REGULAR,    THIRD_GENERATION, "PQ5" },
    {"8948", 30 * GB, MODEL_TYPE_REGULAR,    THIRD_GENERATION, "NLY" },
    {"9245", 40 * GB, MODEL_TYPE_REGULAR,    THIRD_GENERATION, "PNU" },
    
    /* Fourth Generation */
    {"9282", 20 * GB, MODEL_TYPE_REGULAR,    FOURTH_GENERATION, "PS9Q8U" },
    {"9787", 25 * GB, MODEL_TYPE_REGULAR_U2, FOURTH_GENERATION, "V9VW9G" },
    {"9268", 40 * GB, MODEL_TYPE_REGULAR,    FOURTH_GENERATION, "PQ7" },
    {"A079", 20 * GB, MODEL_TYPE_COLOR,      FOURTH_GENERATION, "TDU" },
    {"A127", 20 * GB, MODEL_TYPE_COLOR_U2,   FOURTH_GENERATION, NULL },
    {"9830", 60 * GB, MODEL_TYPE_COLOR,      FOURTH_GENERATION, NULL },
    
    /* First Generation Mini */
    {"9160", 4 * GB, MODEL_TYPE_MINI,        FIRST_GENERATION, "PFW" },
    {"9436", 4 * GB, MODEL_TYPE_MINI_BLUE,   FIRST_GENERATION, "QKLQKQ" },
    {"9435", 4 * GB, MODEL_TYPE_MINI_PINK,   FIRST_GENERATION, "QKKQKP" },
    {"9434", 4 * GB, MODEL_TYPE_MINI_GREEN,  FIRST_GENERATION, NULL },
    {"9437", 4 * GB, MODEL_TYPE_MINI_GOLD,   FIRST_GENERATION, NULL },    

    /* Second Generation Mini */
    {"9800", 4 * GB, MODEL_TYPE_MINI,        SECOND_GENERATION, "S41" },
    {"9802", 4 * GB, MODEL_TYPE_MINI_BLUE,   SECOND_GENERATION, "S43" },
    {"9804", 4 * GB, MODEL_TYPE_MINI_PINK,   SECOND_GENERATION, "S45" },
    {"9806", 4 * GB, MODEL_TYPE_MINI_GREEN,  SECOND_GENERATION, "S47S4J" },
    {"9801", 6 * GB, MODEL_TYPE_MINI,        SECOND_GENERATION, "S42" },
    {"9803", 6 * GB, MODEL_TYPE_MINI_BLUE,   SECOND_GENERATION, "S44" },
    {"9805", 6 * GB, MODEL_TYPE_MINI_PINK,   SECOND_GENERATION, NULL },
    {"9807", 6 * GB, MODEL_TYPE_MINI_GREEN,  SECOND_GENERATION, "S48" },    

    /* Photo / Fourth Generation */
    {"9829", 30 * GB, MODEL_TYPE_COLOR,      FOURTH_GENERATION, "SAY" },
    {"9585", 40 * GB, MODEL_TYPE_COLOR,      FOURTH_GENERATION, "R5Q" },
    {"9586", 60 * GB, MODEL_TYPE_COLOR,      FOURTH_GENERATION, "R5RR5T" },
    {"9830", 60 * GB, MODEL_TYPE_COLOR,      FOURTH_GENERATION, "SAZSB1" },
    
    /* Shuffle / First Generation */
    {"9724", GB / 2, MODEL_TYPE_SHUFFLE,     FIRST_GENERATION, "RS9QGVTSXPFVR80" },
    {"9725", GB,     MODEL_TYPE_SHUFFLE,     FIRST_GENERATION, "RSATSYC60" },
    
    /* Shuffle / Second Generation */
    {"A546", GB,     MODEL_TYPE_SHUFFLE_SILVER, SECOND_GENERATION, "VTE" }, 
    {"A947", GB,     MODEL_TYPE_SHUFFLE_PINK,   SECOND_GENERATION, "XQ5" },
    {"A949", GB,     MODEL_TYPE_SHUFFLE_BLUE,   SECOND_GENERATION, "XQVXQX" },
    {"A951", GB,     MODEL_TYPE_SHUFFLE_GREEN,  SECOND_GENERATION, "XQY" },
    {"A953", GB,     MODEL_TYPE_SHUFFLE_ORANGE, SECOND_GENERATION, "XR1" },

    /* Nano / First Generation */
    {"A350", 1 * GB, MODEL_TYPE_NANO_WHITE,  FIRST_GENERATION, "UNB" },
    {"A352", 1 * GB, MODEL_TYPE_NANO_BLACK,  FIRST_GENERATION, "UPR" },
    {"A004", 2 * GB, MODEL_TYPE_NANO_WHITE,  FIRST_GENERATION, "SZBSZCSZVSZWUNA" },
    {"A099", 2 * GB, MODEL_TYPE_NANO_BLACK,  FIRST_GENERATION, "TJTTJUUPR" },
    {"A005", 4 * GB, MODEL_TYPE_NANO_WHITE,  FIRST_GENERATION, "SZCSZW" },
    {"A107", 4 * GB, MODEL_TYPE_NANO_BLACK,  FIRST_GENERATION, "TK2TK3" },
    
    /* Nano / Second Generation */
    {"A477", 2 * GB, MODEL_TYPE_NANO_SILVER, SECOND_GENERATION, "VQ5VQ6" },
    {"A426", 4 * GB, MODEL_TYPE_NANO_SILVER, SECOND_GENERATION, "V8TV8U" },
    {"A428", 4 * GB, MODEL_TYPE_NANO_BLUE,   SECOND_GENERATION, "V8WV8X" },
    {"A487", 4 * GB, MODEL_TYPE_NANO_GREEN,  SECOND_GENERATION, "VQHVQJ" },
    {"A489", 4 * GB, MODEL_TYPE_NANO_PINK,   SECOND_GENERATION, "VQKVQL" },
    {"A725", 4 * GB, MODEL_TYPE_NANO_PRODUCT_RED, SECOND_GENERATION, "WL2WL3" },
    {"A726", 8 * GB, MODEL_TYPE_NANO_PRODUCT_RED, SECOND_GENERATION, "X9AX9B" },
    {"A497", 8 * GB, MODEL_TYPE_NANO_BLACK,  SECOND_GENERATION, "VQTVQU" },

    /* Video / Fifth Generation */
    {"A002", 30 * GB, MODEL_TYPE_VIDEO_WHITE, FIFTH_GENERATION, "SZ9SZTSZUWECWEDWEGWEHWEL" },
    {"A146", 30 * GB, MODEL_TYPE_VIDEO_BLACK, FIFTH_GENERATION, "TXKTXLTXMTXNWEEWEFWEJWEK" },
    {"A003", 60 * GB, MODEL_TYPE_VIDEO_WHITE, FIFTH_GENERATION, "SZASZTSZUWECWEDWEGWEHWEL" },
    {"A147", 60 * GB, MODEL_TYPE_VIDEO_BLACK, FIFTH_GENERATION, "TXKTXLTXMTXNWEEWEFWEJWEK" },
    
    /* Video / Late 2006 Fifth Generation */
    {"A444", 30 * GB, MODEL_TYPE_VIDEO_WHITE, FIFTH_GENERATION, "V9KV9LWU9" }, /* Are the W?? U2's? */
    {"A446", 30 * GB, MODEL_TYPE_VIDEO_BLACK, FIFTH_GENERATION, "VQMV9MV9N" },
    {"A448", 80 * GB, MODEL_TYPE_VIDEO_BLACK, FIFTH_GENERATION, "V9SV9PV9R" },
    {"A450", 80 * GB, MODEL_TYPE_VIDEO_BLACK, FIFTH_GENERATION, "WUC" },  /* Possibly Video U2? */
    {"A446", 30 * GB, MODEL_TYPE_VIDEO_BLACK, FIFTH_GENERATION, "W9G" },  /* Video U2 */

    /* HP iPods, need contributions for this table */
    {"E436", 40 * GB, MODEL_TYPE_REGULAR, FOURTH_GENERATION, NULL },
    
    {NULL, 0, 0, 0}
};

static const gchar *ipod_model_name_table [] = {
    "Invalid",
    "Unknown",
    "Color",
    "Color U2",
    "Grayscale",
    "Grayscale U2",
    "Mini (Silver)",
    "Mini (Blue)",
    "Mini (Pink)",
    "Mini (Green)",
    "Mini (Gold)",
    "Shuffle",
    "Nano (White)",
    "Nano (Black)",
    "Video (White)",
    "Video (Black)",
    "Nano (Silver)",
    "Nano (Green)",
    "Nano (Blue)",
    "Nano (Pink)",
    NULL
};


