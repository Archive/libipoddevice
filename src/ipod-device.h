/* ex: set ts=4: */
/***************************************************************************
*  ipod-device.h
*  Copyright (C) 2005-2005 Novell, Inc.
*  Written by Aaron Bockover <aaron@aaronbock.net>
****************************************************************************/

/*  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of version 2.1 of the GNU Lesser General Public
 *  License as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */

#ifndef IPOD_DEVICE_H
#define IPOD_DEVICE_H

#include <glib.h>
#include <glib-object.h>

#include <libhal.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

#define TYPE_IPOD_DEVICE         (ipod_device_get_type ())
#define IPOD_DEVICE(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), TYPE_IPOD_DEVICE, IpodDevice))
#define IPOD_DEVICE_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), TYPE_IPOD_DEVICE, IpodDeviceClass))
#define IS_IPOD_DEVICE(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), TYPE_IPOD_DEVICE))
#define IS_IPOD_DEVICE_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), TYPE_IPOD_DEVICE))
#define IPOD_DEVICE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), TYPE_IPOD_DEVICE, IpodDeviceClass))

typedef struct IpodDevicePrivate IpodDevicePrivate;

typedef struct {
    GObject parent;
    IpodDevicePrivate *priv;
} IpodDevice;

typedef struct {
    GObjectClass parent_class;
} IpodDeviceClass;

enum {
    UNKNOWN_GENERATION,
    FIRST_GENERATION,
    SECOND_GENERATION,
    THIRD_GENERATION,
    FOURTH_GENERATION,
    FIFTH_GENERATION
};

enum {
    MODEL_TYPE_INVALID,
    MODEL_TYPE_UNKNOWN,
    MODEL_TYPE_COLOR,
    MODEL_TYPE_COLOR_U2,
    MODEL_TYPE_REGULAR,
    MODEL_TYPE_REGULAR_U2,
    MODEL_TYPE_MINI,
    MODEL_TYPE_MINI_BLUE,
    MODEL_TYPE_MINI_PINK,
    MODEL_TYPE_MINI_GREEN,
    MODEL_TYPE_MINI_GOLD,
    MODEL_TYPE_SHUFFLE,
    MODEL_TYPE_NANO_WHITE,
    MODEL_TYPE_NANO_BLACK,
    MODEL_TYPE_VIDEO_WHITE,
    MODEL_TYPE_VIDEO_BLACK,
	MODEL_TYPE_NANO_SILVER,
	MODEL_TYPE_NANO_GREEN,
	MODEL_TYPE_NANO_BLUE,
	MODEL_TYPE_NANO_PINK,
	MODEL_TYPE_NANO_PRODUCT_RED,
	MODEL_TYPE_SHUFFLE_SILVER,
	MODEL_TYPE_SHUFFLE_PINK,
	MODEL_TYPE_SHUFFLE_BLUE,
	MODEL_TYPE_SHUFFLE_GREEN,
	MODEL_TYPE_SHUFFLE_ORANGE
};

enum {
    EJECT_OK,
    EJECT_ERROR,
    EJECT_BUSY
};

enum {
    PROP_IPOD_0,
    PROP_HAL_VOLUME_ID,
    PROP_HAL_CONTEXT,
    PROP_MOUNT_POINT,
    PROP_DEVICE_PATH,
    PROP_CONTROL_PATH,
    PROP_DEVICE_MODEL,
    PROP_DEVICE_MODEL_STRING,
    PROP_DEVICE_GENERATION,
    PROP_ADVERTISED_CAPACITY,
    PROP_DEVICE_NAME,
    PROP_USER_NAME,
    PROP_HOST_NAME,
    PROP_VOLUME_SIZE,
    PROP_VOLUME_AVAILABLE,
    PROP_VOLUME_USED,
    PROP_IS_IPOD,
    PROP_IS_NEW,
    PROP_SERIAL_NUMBER,
    PROP_MODEL_NUMBER,
    PROP_FIRMWARE_VERSION,
    PROP_VOLUME_UUID,
    PROP_VOLUME_LABEL,
    PROP_CAN_WRITE,
    PROP_ARTWORK_FORMAT,
    PROP_MANUFACTURER_ID,
    PROP_PRODUCTION_YEAR,
    PROP_PRODUCTION_WEEK,
    PROP_PRODUCTION_INDEX
};

enum {
    ERROR_SAVE,
    ERROR_EJECT
};

typedef enum {
    IPOD_ARTWORK_UNKNOWN = -1,
    IPOD_ARTWORK_PHOTO,
    IPOD_ARTWORK_COVER
} IpodArtworkUsage;

typedef enum {
    IPOD_PIXEL_FORMAT_UNKNOWN = -1,
    IPOD_PIXEL_FORMAT_RGB565,
    IPOD_PIXEL_FORMAT_RGB565_BE,
    IPOD_PIXEL_FORMAT_IYUV
} IpodPixelFormat;

typedef struct {
    IpodArtworkUsage usage;
    gint16 width;
    gint16 height;
    gint16 correlation_id;
    gint image_size;
    IpodPixelFormat pixel_format;
    gint16 rotation;
} IpodArtworkFormat;


GType ipod_device_get_type();
IpodDevice *ipod_device_new(const gchar *mount_point);
gboolean ipod_device_rescan_disk(IpodDevice *device);
guint ipod_device_eject(IpodDevice *device, GError **error);
void ipod_device_debug(IpodDevice *device);
gboolean ipod_device_save(IpodDevice *device, GError **error);

GList *ipod_device_list_devices();
GList *ipod_device_list_device_udis();

void ipod_device_dump_model_table_as_xml();

G_END_DECLS

#endif /* IPOD_DEVICE_H */
