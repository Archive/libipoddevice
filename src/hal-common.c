/* ex: set ts=4: */
/***************************************************************************
*  hal-common.c
*  Copyright (C) 2005 Novell 
*  Written by Aaron Bockover <aaron@aaronbock.net>
****************************************************************************/

/*  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of version 2.1 of the GNU Lesser General Public
 *  License as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */

#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "hal-common.h"

static GMainContext *ipod_device_global_main_context = NULL;

dbus_bool_t 
hal_mainloop_integration(LibHalContext *ctx, DBusError *error)
{
    DBusConnection *dbus_connection;
    
    dbus_connection = dbus_bus_get(DBUS_BUS_SYSTEM, error);
    
    if(dbus_error_is_set(error))
        return FALSE;
    
    dbus_connection_setup_with_g_main(dbus_connection, 
        ipod_device_global_main_context);
    libhal_ctx_set_dbus_connection(ctx, dbus_connection);
    
    return TRUE;
}

void ipod_device_set_global_main_context(GMainContext *ctx)
{
    ipod_device_global_main_context = ctx;
}
