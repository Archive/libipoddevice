/* ex: set ts=4: */
/***************************************************************************
*  ipod-device.c
*  Copyright (C) 2005 Novell 
*  Written by Aaron Bockover <aaron@aaronbock.net>
****************************************************************************/

/*  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of version 2.1 of the GNU Lesser General Public
 *  License as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "hal-common.h"
#include "ipod-device-event-listener.h"
#include "ipod-device.h"

static GList *instances = NULL;

#define g_free_if_not_null(o) \
    if(o != NULL) {           \
        g_free(o);            \
        o = NULL;             \
    }
    
static void ipod_device_event_listener_class_init(IpodDeviceEventListenerClass *klass);
static void ipod_device_event_listener_init(IpodDeviceEventListener *sp);
static void ipod_device_event_listener_finalize(GObject *object);

void ipod_device_event_listener_hal_init(IpodDeviceEventListener *listener);
gboolean
ipod_device_event_listener_udi_is_ipod(LibHalContext *hal_ctx, const char *udi, gboolean *mounted);    
/* Signal Prototypes */
    
static void ipod_device_event_listener_device_added(IpodDeviceEventListener *listener, const gchar *udi);
static void ipod_device_event_listener_device_removed(IpodDeviceEventListener *listener, const gchar *udi);
static void ipod_device_event_listener_notify_device_added(IpodDeviceEventListener *listener);
static void ipod_device_event_listener_notify_device_removed(IpodDeviceEventListener *listener);
    
struct IpodDeviceEventListenerPrivate {
    LibHalContext *ctx;
    GMainContext *main_context;
    GList *ipodUdis;
};

static GObjectClass *parent_class = NULL;

static guint ipod_device_event_listener_signals[LAST_SIGNAL] = {0, 0, 0, 0};

gint g_list_find_string_index(GList *list, const gchar *str)
{
    gchar *match_str;
    gint i, n;
    
    for(i = 0, n = g_list_length(list); i < n; i++) {
        match_str = g_list_nth_data(list, i);
        if(match_str != NULL && strcmp(str, match_str) == 0)
            return i;
    }
    
    return -1;
}

/* GObject Class Specific Methods */

GType
ipod_device_event_listener_get_type()
{
    static GType type = 0;

    if(type == 0) {
        static const GTypeInfo our_info = {
            sizeof (IpodDeviceEventListenerClass),
            NULL,
            NULL,
            (GClassInitFunc)ipod_device_event_listener_class_init,
            NULL,
            NULL,
            sizeof (IpodDeviceEventListener),
            0,
            (GInstanceInitFunc)ipod_device_event_listener_init,
        };

        type = g_type_register_static(G_TYPE_OBJECT, 
            "IpodDeviceEventListener", &our_info, 0);
    }

    return type;
}

static void
ipod_device_event_listener_class_init(IpodDeviceEventListenerClass *klass)
{
    GObjectClass *class = G_OBJECT_CLASS(klass);
    parent_class = g_type_class_peek_parent(klass);
    
    class->finalize = ipod_device_event_listener_finalize;
    
    klass->device_added = ipod_device_event_listener_device_added;
    klass->device_removed = ipod_device_event_listener_device_removed;
    klass->notify_device_added = ipod_device_event_listener_notify_device_added;
    klass->notify_device_removed = 
        ipod_device_event_listener_notify_device_removed;
    
    ipod_device_event_listener_signals[DEVICE_ADDED] = 
        g_signal_new("device-added",
            TYPE_IPOD_DEVICE_EVENT_LISTENER,
            G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
            G_STRUCT_OFFSET(IpodDeviceEventListenerClass, device_added),
            NULL,
            NULL,
            g_cclosure_marshal_VOID__STRING,
            G_TYPE_NONE,
            1, G_TYPE_STRING);

    ipod_device_event_listener_signals[DEVICE_REMOVED] = 
        g_signal_new("device-removed",
            TYPE_IPOD_DEVICE_EVENT_LISTENER,
            G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
            G_STRUCT_OFFSET(IpodDeviceEventListenerClass, device_removed),
            NULL,
            NULL,
            g_cclosure_marshal_VOID__STRING,
            G_TYPE_NONE,
            1, G_TYPE_STRING);
            
    ipod_device_event_listener_signals[NOTIFY_DEVICE_ADDED] = 
        g_signal_new("notify-device-added",
            TYPE_IPOD_DEVICE_EVENT_LISTENER,
            G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
            G_STRUCT_OFFSET(IpodDeviceEventListenerClass, notify_device_added),
            NULL,
            NULL,
            g_cclosure_marshal_VOID__VOID,
            G_TYPE_NONE,
            0);
            
    ipod_device_event_listener_signals[NOTIFY_DEVICE_REMOVED] = 
        g_signal_new("notify-device-removed",
            TYPE_IPOD_DEVICE_EVENT_LISTENER,
            G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
            G_STRUCT_OFFSET(IpodDeviceEventListenerClass, notify_device_removed),
            NULL,
            NULL,
            g_cclosure_marshal_VOID__VOID,
            G_TYPE_NONE,
            0);
}

static void
ipod_device_event_listener_init(IpodDeviceEventListener *listener)
{
    GList *devices;
    int i, n;
    
    listener->priv = g_new0(IpodDeviceEventListenerPrivate, 1);
    listener->priv->ipodUdis = NULL;
    instances = g_list_append(instances, listener);
    ipod_device_event_listener_hal_init(listener);

    devices = ipod_device_list_device_udis();
    for(i = 0, n = g_list_length(devices); i < n; i++) {
        gchar *udi = (gchar *)g_list_nth_data(devices, i);
        listener->priv->ipodUdis = g_list_append(listener->priv->ipodUdis, 
            g_strdup(udi));
        g_free(udi);
    }
    g_list_free(devices);
}

static void
ipod_device_event_listener_finalize(GObject *object)
{
    IpodDeviceEventListener *listener = IPOD_DEVICE_EVENT_LISTENER(object);
    gint i, n;
    
    g_free(listener->priv);
    G_OBJECT_CLASS(parent_class)->finalize(object);
    
    for(i = 0, n = g_list_length(listener->priv->ipodUdis); i < n; i++)
        g_free(g_list_nth_data(listener->priv->ipodUdis, i));
    
    g_list_free(listener->priv->ipodUdis);
    
    instances = g_list_remove(instances, listener);
    if(g_list_length(instances) == 0) {
        g_list_free(instances);
        instances = NULL;
    }
}

static void 
ipod_device_event_listener_device_added(IpodDeviceEventListener *listener, 
    const gchar *udi)
{

}

static void 
ipod_device_event_listener_device_removed(IpodDeviceEventListener *listener, 
    const gchar *udi)
{

}

static void 
ipod_device_event_listener_notify_device_added(IpodDeviceEventListener 
    *listener)
{

}

static void 
ipod_device_event_listener_notify_device_removed(IpodDeviceEventListener
    *listener)
{

}

static void
add_device(const char *udi)
{
    int i, n;
    
    for(i = 0, n = g_list_length(instances); i < n; i++) {
        IpodDeviceEventListener *listener = g_list_nth_data(instances, i);
        
        if(g_list_find_string_index(listener->priv->ipodUdis, udi) < 0) {
            listener->priv->ipodUdis = 
                g_list_append(listener->priv->ipodUdis, g_strdup(udi));

            g_signal_emit_by_name(listener, "notify-device-added");
            g_signal_emit_by_name(listener, "device-added", udi, NULL);
        }
    }
}

static void
remove_device(const char *udi)
{
    gchar *matchUdi;
    gint i, n, index;

    for(i = 0, n = g_list_length(instances); i < n; i++) {
        IpodDeviceEventListener *listener = g_list_nth_data(instances, i);
        
        if((index = g_list_find_string_index(listener->priv->ipodUdis, 
            udi)) >= 0) {
            g_signal_emit_by_name(listener, "notify-device-removed");
            g_signal_emit_by_name(listener, "device-removed", udi, NULL);
            matchUdi = g_list_nth_data(listener->priv->ipodUdis, index);
            listener->priv->ipodUdis = g_list_remove(listener->priv->ipodUdis, 
                    matchUdi);
            g_free(matchUdi);
        }
    }
}

static void
hal_device_added(LibHalContext *ctx, 
    const char *udi)
{
    gboolean mounted = FALSE;
    
    if(!ipod_device_event_listener_udi_is_ipod(ctx, udi, &mounted) || !mounted)
        return;
    
    add_device (udi);
}

static void
hal_device_removed(LibHalContext *ctx __attribute__((__unused__)), 
    const char *udi)
{
    remove_device(udi);
}

static void
hal_device_property_modified (LibHalContext *ctx, const char *udi, 
    const char *key, dbus_bool_t is_removed, dbus_bool_t is_added)
{
    gboolean mounted = FALSE;
    
    if (strcmp(key, "volume.is_mounted") != 0)
        return;

    if (ipod_device_event_listener_udi_is_ipod(ctx, udi, &mounted) && mounted)
        add_device(udi);
    else
        remove_device(udi);
}

void 
ipod_device_event_listener_hal_init(IpodDeviceEventListener *listener)
{
    LibHalContext *hal_context;
    char **devices;
    gint device_count;
    DBusError error;
    
    if(!(hal_context = libhal_ctx_new()))
        return;
    
    dbus_error_init(&error);
    if(!hal_mainloop_integration(hal_context, &error)) {
        dbus_error_free(&error);
        libhal_ctx_free(hal_context);
        return;
    }
    
    libhal_ctx_set_device_added(hal_context, hal_device_added);
    libhal_ctx_set_device_removed(hal_context, hal_device_removed);
    libhal_ctx_set_device_property_modified(hal_context, 
        hal_device_property_modified);
    
    if(!libhal_device_property_watch_all(hal_context, &error)) {
        dbus_error_free(&error);
        libhal_ctx_free(hal_context);
        return;
    }

    if(!libhal_ctx_init(hal_context, &error)) {
        dbus_error_free(&error);
        libhal_ctx_free(hal_context);
        return;
    }
    
    devices = libhal_get_all_devices(hal_context, &device_count, NULL);
    if(devices == NULL) {
        libhal_ctx_shutdown(hal_context, NULL);
        libhal_ctx_free(hal_context);
        hal_context = NULL;
        return;
    }
    
    libhal_free_string_array(devices);
    listener->priv->ctx = hal_context;
}

gboolean
ipod_device_event_listener_udi_is_ipod(LibHalContext *hal_ctx, const char *udi, 
    gboolean *mounted)
{
    gchar *product = NULL;
    gchar *storage_device = NULL;
    gchar **volumes;
    gint i, volume_count;
    gboolean is_ipod = FALSE;

    *mounted = FALSE;
    
    if(!(storage_device = libhal_device_get_property_string(hal_ctx, udi, 
        "block.storage_device", NULL))) {
        libhal_free_string(storage_device);
        return FALSE;
    }
    
    if(!(product = libhal_device_get_property_string(hal_ctx, storage_device, 
        "info.product", NULL))) {
        libhal_free_string(storage_device);
        libhal_free_string(product);
        return FALSE;
    }
    
    if(g_strcasecmp(product, "iPod") == 0) {
        is_ipod = TRUE;
        
        volumes = libhal_manager_find_device_string_match(hal_ctx, 
            "info.category", "volume", &volume_count, NULL);
            
        for(i = 0; i < volume_count; i++) {
            if(!libhal_device_property_exists(hal_ctx, 
                volumes[i], "volume.is_mounted", NULL) || 
                !libhal_device_get_property_bool(hal_ctx, 
                volumes[i], "volume.is_mounted", NULL)) {
                continue;
            }
            
            if(g_strcasecmp(udi, volumes[i]) == 0) {
                *mounted = TRUE;
                break;
            }
        }
        
        libhal_free_string_array(volumes);
    }
    
    libhal_free_string(storage_device);
    libhal_free_string(product);
    
    return is_ipod;
}

/* PUBLIC METHODS */

IpodDeviceEventListener *
ipod_device_event_listener_new()
{
    IpodDeviceEventListener *listener = g_object_new(
        TYPE_IPOD_DEVICE_EVENT_LISTENER, NULL);
    
    if(listener->priv->ctx == NULL) {
        g_object_unref(listener);
        return NULL;
    }
    
    return listener;
}
