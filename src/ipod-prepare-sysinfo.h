#ifndef _IPOD_PREPARE_SYSINFO
#define _IPOD_PREPARE_SYSINFO

#include <glib.h>

gboolean
ipod_prepare_sysinfo(gchar *serial, gboolean can_write, gchar *mount_point, gchar **output_path);

#endif /* _IPOD_PREPARE_SYSINFO */

