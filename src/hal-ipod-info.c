#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <scsi/sg_cmds.h>
#include <libhal.h>
#include <glib.h>

#include "glib-compat.h"
#include "ipod-prepare-sysinfo.h"

#define IPOD_BUF_LENGTH 252
#define IPOD_SERIAL_PAGE 0x80
#define IPOD_XML_PAGE 0xC0

static unsigned char
hal_ipod_do_inquiry (int fd, int page, char *buf, char **start)
{
	if (sg_ll_inquiry (fd, 0, 1, page, buf, IPOD_BUF_LENGTH, 1, 0) != 0) {
		*start = NULL;
		return 0;
	} else {
		*start = buf + 4;
		return (unsigned char)buf[3];
	}
}

/* taken from libipoddevice proper */
static LibHalContext *
hal_ipod_initialize()
{
    LibHalContext *hal_context;
    DBusError error;
    DBusConnection *dbus_connection;
    char **devices;
    int device_count;

    hal_context = libhal_ctx_new();
    if(hal_context == NULL) {
        return NULL;
    }
    
    dbus_error_init(&error);
    dbus_connection = dbus_bus_get(DBUS_BUS_SYSTEM, &error);
    if(dbus_error_is_set(&error)) {
        dbus_error_free(&error);
        libhal_ctx_free(hal_context);
        return NULL;
    }
    
    libhal_ctx_set_dbus_connection(hal_context, dbus_connection);
    
    if(!libhal_ctx_init(hal_context, &error)) {
        libhal_ctx_free(hal_context);
        return NULL;
    }

    devices = libhal_get_all_devices(hal_context, &device_count, NULL);
    if(devices == NULL) {
        libhal_ctx_shutdown(hal_context, NULL);
        libhal_ctx_free(hal_context);
        hal_context = NULL;
        return NULL;
    }
    
    libhal_free_string_array(devices);
    
    return hal_context;
}

static int
hal_ipod_open_device (void)
{
	return open(getenv ("HAL_PROP_BLOCK_DEVICE"), O_RDWR);
}

static int
hal_ipod_set_serial (void)
{
	int fd;
	char buf[512];
	int len;
	LibHalContext *ctx;
	char *start;

	fd = hal_ipod_open_device ();
	if (fd <= 0) {
		fprintf (stderr, "Failed to open device\n");
		return 1;
	}
	
	len = hal_ipod_do_inquiry (fd, IPOD_SERIAL_PAGE, buf, &start);
	if (start == NULL || len == 0) {
		return 0;
	}

	ctx = hal_ipod_initialize ();
	if (ctx == NULL) {
		fprintf (stderr, "Failed to initialized hal context\n");
		return 1;
	}
	
	libhal_device_set_property_string (ctx, getenv("UDI"), "info.ipod.serial_number",
					   start, NULL);
	libhal_ctx_free (ctx);
	return 0;
}

static int
hal_ipod_write_xml (void)
{
	int fd;
	int i;
	int readonly;
	unsigned char len;
	char buf[512];
	char *output_path;
	char *offsets;
	char *serial;
	char *mount_point;
	char *start;
	FILE *output;
	LibHalContext *ctx;

	ctx = hal_ipod_initialize ();
	if (ctx == NULL) {
		fprintf(stderr, "Failed to initialize HAL context\n");
		return 1;
	}

	readonly = 0;
	if (!libhal_device_property_exists(ctx, getenv("UDI"),
		"volume.is_mounted_read_only", NULL)) {
		readonly = libhal_device_get_property_bool(ctx, getenv("UDI"),
			"volume.is_mounted_read_only", NULL);
	}

	libhal_ctx_free(ctx);

	fd = hal_ipod_open_device ();
	if (fd <= 0) {
		fprintf (stderr, "Failed to open device\n");
		return 0;
	}

	len = hal_ipod_do_inquiry (fd, IPOD_SERIAL_PAGE, buf, &start);
	if (start == NULL || len == 0) {
		close(fd);
		return 1;
	}

	serial = g_strdup(start);

	len = hal_ipod_do_inquiry (fd, IPOD_XML_PAGE, buf, &start);
	if (start == NULL || len == 0) {
		close(fd);
		return 1;
	}

	mount_point = getenv("HAL_PROP_VOLUME_MOUNT_POINT");
	if (!ipod_prepare_sysinfo (serial, !readonly, mount_point, &output_path)) {
		free(serial);
		close(fd);
		return 1;
	}

	offsets = malloc(sizeof(char) * len);
	memcpy(offsets, start, len + 1);
	offsets[len] = 0;

	output = fopen (output_path, "w");
	if (output == NULL) {
		fprintf(stderr, "Failed to create output file\n");
		free(serial);
		free(offsets);
		free(output_path);
		close(fd);
		return 1;
	}
	
	for (i = 0; offsets[i]; i++) {
		bzero(buf, 512);
		len = hal_ipod_do_inquiry (fd, offsets[i], buf, &start);
		start[len] = 0;
		fprintf (output, "%s", start);
	}

	free(serial);
	free(offsets);
	free(output_path);
	fclose(output);
	close(fd);
	
	return 0;
}

int
main()
{
	if (getenv ("HAL_PROP_BLOCK_DEVICE") == NULL) {
		fprintf(stderr, "Failed to get block device\n");
		return 1;
	}
	
	if (getenv ("HALD_ACTION") != NULL) {
		return hal_ipod_set_serial ();
	} else {
		return hal_ipod_write_xml ();
	}
}

