/* ex: set ts=4: */
/***************************************************************************
*  hal-common.h
*  Copyright (C) 2005 Novell 
*  Written by Aaron Bockover <aaron@aaronbock.net>
****************************************************************************/

/*  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of version 2.1 of the GNU Lesser General Public
 *  License as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */

#ifndef HAL_COMMON_H
#define HAL_COMMON_H

#include <glib.h>
#include <glib-object.h>

#include <libhal.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>

dbus_bool_t hal_mainloop_integration(LibHalContext *ctx, DBusError *error);
void ipod_device_set_global_main_context(GMainContext *ctx);

#endif /* HAL_COMMON_H */
