#include <glib.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include "ipod-prepare-sysinfo.h"

gboolean
ipod_prepare_sysinfo(gchar *serial, gint can_write, gchar *mount_point, gchar **filename)
{
    gchar *directory;
    gchar *_filename;
    
    *filename = NULL;

    if(can_write) {
        directory = g_build_filename(mount_point, "iPod_Control", "Device", NULL);
        _filename = g_build_filename(directory, "SysInfoExtended", NULL);
    } else {
        directory = g_strdup_printf("/tmp");
        _filename = g_strdup_printf("%s/ipod-sysinfo-%s", directory, serial);
    }

    if(directory == NULL || _filename == NULL) {
        fprintf(stderr, "Failed to build output path\n");
        return FALSE;
    }
    
    if(g_mkdir_with_parents(directory, 0755) == -1) {
        fprintf(stderr, "Failed to create device directory (g_mkdir_with_parents)\n");
        free(directory);
        free(_filename);
        return FALSE;
    }

    *filename = _filename;
    g_free(directory);

    return TRUE;
}

