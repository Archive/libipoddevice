/* ex: set ts=4: */
/***************************************************************************
*  ipod-device.c
*  Copyright (C) 2005-2006 Novell, Inc. 
*  Written by Aaron Bockover <aaron@abock.org>
*             James Willcox <snorp@snorp.net>
****************************************************************************/

/*  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of version 2.1 of the GNU Lesser General Public
 *  License as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <glib/gstdio.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>

#include <glibtop/fsusage.h>

#include "hal-common.h"
#include "ipod-device.h"
#include "glib-compat.h"
#include "ipod-model-table.h"
#include "ipod-prepare-sysinfo.h"

#define g_free_if_not_null(o) \
    if(o != NULL) {           \
        g_free(o);            \
        o = NULL;             \
    }
    
#define CAPACITY_DISPLAY_STRING(cap)   \
    g_strdup_printf("%d %s",           \
        cap < 1024 ? cap : cap / 1024, \
        cap < 1024 ? "MB" : "GB")

static void ipod_device_class_init(IpodDeviceClass *klass);
static void ipod_device_init(IpodDevice *sp);
static void ipod_device_finalize(GObject *object);

static gchar *ipod_device_read_device_info_string(FILE *fd);
static void ipod_device_write_device_info_string(gchar *str, FILE *fd);
    
static gboolean ipod_device_reload(IpodDevice *device);
static void ipod_device_construct_paths(IpodDevice *device);
static gboolean ipod_device_info_load(IpodDevice *device);
static guint ipod_device_detect_model(IpodDevice *device);
static void ipod_device_parse_serial(IpodDevice *device);
static gboolean ipod_device_detect_volume_info(IpodDevice *device);
static LibHalContext *ipod_device_hal_initialize();
static void ipod_device_detect_volume_used(IpodDevice *device);
static gboolean ipod_device_read_sysinfo(IpodDevice *device);
static gboolean ipod_device_detect_writeable(IpodDevice *device);
    
typedef struct _IpodSerialNumber {
    gchar *serial;
    gchar production_factory[3];
    gint production_year;
    gint production_week;
    gint production_number;
    gchar model_code[4];
} IpodSerialNumber;
    
struct IpodDevicePrivate {
    LibHalContext *hal_context;
    
    /* Paths */
    gchar *device_path;
    gchar *mount_point;
    gchar *control_path;
    gchar *hal_volume_id;
    
    gchar *adv_capacity;
    guint model_index;

    /* DeviceInfo Fields (All Devices) */ 
    gchar *device_name;
    gchar *user_name;
    gchar *host_name;
    
    /* SysInfo+Extended Fields */
    gchar *model_number;
    gchar *firmware_version;
    IpodSerialNumber serial_number;

    /* Artwork Formats */
    IpodArtworkFormat *artwork_formats;
    
    /* Volume Size/Usage */
    guint64 volume_size;
    guint64 volume_available;
    guint64 volume_used;
    
    gchar *volume_uuid;
    gchar *volume_label;
    
    /* Fresh from the factory/restore? */
    gboolean is_new;
    
    /* Safety */
    gboolean is_ipod;
    gboolean can_write;
};

static GObjectClass *parent_class = NULL;

/* GObject Class Specific Methods */

GType
ipod_device_get_type()
{
    static GType type = 0;

    if(type == 0) {
        static const GTypeInfo our_info = {
            sizeof (IpodDeviceClass),
            NULL,
            NULL,
            (GClassInitFunc)ipod_device_class_init,
            NULL,
            NULL,
            sizeof (IpodDevice),
            0,
            (GInstanceInitFunc)ipod_device_init,
        };

        type = g_type_register_static(G_TYPE_OBJECT, 
            "IpodDevice", &our_info, 0);
    }

    return type;
}

static gchar *
ipod_version_string_from_hex(glong version)
{
    gint major, minor, micro;
    
    version >>= 16;
    major = (version & 0x0f00) >> 8;
    minor = (version & 0x00f0) >> 4;
    micro = version & 0x000f;
    
    return micro == 0 ?
        g_strdup_printf("%d.%d", major, minor) :
        g_strdup_printf("%d.%d.%d", major, minor, micro); 
}

static gchar *
ipod_version_string_from_hex_string(gchar *hex_version)
{
    if(hex_version == NULL) {
        return NULL;
    } 
    
    return ipod_version_string_from_hex(g_ascii_strtoull(hex_version, NULL, 16));
}

static void 
ipod_device_get_property(GObject *object, guint prop_id, 
    GValue *value, GParamSpec *pspec)
{
    IpodDevice *device = IPOD_DEVICE(object);
    
    switch(prop_id) {
        case PROP_HAL_CONTEXT:
            g_value_set_pointer(value, device->priv->hal_context);
            break;
        case PROP_HAL_VOLUME_ID:
            g_value_set_string(value, device->priv->hal_volume_id);
            break;
        case PROP_MOUNT_POINT:
            g_value_set_string(value, device->priv->mount_point);
            break;
        case PROP_DEVICE_PATH:
            g_value_set_string(value, device->priv->device_path);
            break;
        case PROP_CONTROL_PATH:
            g_value_set_string(value, device->priv->control_path);
            break;
        case PROP_DEVICE_MODEL:
            g_value_set_uint(value, 
                ipod_model_table[device->priv->model_index].model_type);
            break;
        case PROP_DEVICE_MODEL_STRING:
            g_value_set_string(value,
                ipod_model_name_table[ipod_model_table[device->priv->model_index].model_type]);
            break;
        case PROP_DEVICE_GENERATION:
            g_value_set_uint(value, 
                ipod_model_table[device->priv->model_index].generation);
            break;
        case PROP_ADVERTISED_CAPACITY:
            g_value_set_string(value, device->priv->adv_capacity);
            break;
        case PROP_DEVICE_NAME:
            g_value_set_string(value, device->priv->device_name);
            break;
        case PROP_USER_NAME:
            g_value_set_string(value, device->priv->user_name);
            break;
        case PROP_HOST_NAME:
            g_value_set_string(value, device->priv->host_name);
            break;
        case PROP_VOLUME_SIZE:
            g_value_set_uint64(value, device->priv->volume_size);
            break;
        case PROP_VOLUME_AVAILABLE:
            g_value_set_uint64(value, device->priv->volume_available);
            break;
        case PROP_VOLUME_USED:
            g_value_set_uint64(value, device->priv->volume_used);
            break;
        case PROP_IS_IPOD:
            g_value_set_boolean(value, device->priv->is_ipod);
            break;
        case PROP_IS_NEW:
            g_value_set_boolean(value, device->priv->is_new);
            break;
        case PROP_SERIAL_NUMBER:
            g_value_set_string(value, device->priv->serial_number.serial);
            break;
        case PROP_MODEL_NUMBER:
            g_value_set_string(value, device->priv->model_number);
            break;
        case PROP_FIRMWARE_VERSION: {
            gchar *firmware_formatted = ipod_version_string_from_hex_string(device->priv->firmware_version);
            g_value_set_string(value, firmware_formatted);
            if(firmware_formatted != NULL) {
                g_free(firmware_formatted);
            }
            break;
        } case PROP_VOLUME_UUID:
            g_value_set_string(value, device->priv->volume_uuid);
            break;
        case PROP_VOLUME_LABEL:
            g_value_set_string(value, device->priv->volume_label);
            break;
        case PROP_CAN_WRITE:
            g_value_set_boolean(value, device->priv->can_write);
            break;
        case PROP_ARTWORK_FORMAT:
            g_value_set_pointer(value, (gpointer) device->priv->artwork_formats);
            break;
        case PROP_MANUFACTURER_ID:
            g_value_set_string(value, device->priv->serial_number.production_factory);
            break;
        case PROP_PRODUCTION_YEAR:
            g_value_set_uint(value, device->priv->serial_number.production_year);
            break;
        case PROP_PRODUCTION_WEEK:
            g_value_set_uint(value, device->priv->serial_number.production_week);
            break;
        case PROP_PRODUCTION_INDEX:
            g_value_set_uint(value, device->priv->serial_number.production_number);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
ipod_device_set_property(GObject *object, guint prop_id,
    const GValue *value, GParamSpec *pspec)
{
    IpodDevice *device = IPOD_DEVICE(object);
    const gchar *str;
    gchar **volumes;
    gint volume_count;
    
    switch(prop_id) {
        case PROP_MOUNT_POINT:
        case PROP_DEVICE_PATH:
        case PROP_HAL_VOLUME_ID:
            str = g_value_get_string(value);
            
            volumes = libhal_manager_find_device_string_match(
                device->priv->hal_context, "block.device", str, 
                &volume_count, NULL);
        
            if(volume_count == 0) {
                libhal_free_string_array(volumes);
                volumes = libhal_manager_find_device_string_match(
                    device->priv->hal_context, "volume.mount_point", 
                    str, &volume_count, NULL);
                
                if(volume_count >= 1)
                    str = volumes[0];
            } else {
                str = volumes[0];
            }
            
            g_free_if_not_null(device->priv->hal_volume_id);
            device->priv->hal_volume_id = g_strdup(str);
            device->priv->is_ipod = ipod_device_reload(device);
            
            libhal_free_string_array(volumes);    
            
            break;
        case PROP_DEVICE_NAME:
            str = g_value_get_string(value);
            g_free_if_not_null(device->priv->device_name);
            device->priv->device_name = g_strdup(str);
            break;
        case PROP_USER_NAME:
            str = g_value_get_string(value);
            g_free_if_not_null(device->priv->user_name);
            device->priv->user_name = g_strdup(str);
            break;
        case PROP_HOST_NAME:
            str = g_value_get_string(value);
            g_free_if_not_null(device->priv->host_name);
            device->priv->host_name = g_strdup(str);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
ipod_device_class_init(IpodDeviceClass *klass)
{
    GParamSpec *hal_context_param;
    GParamSpec *hal_volume_id_param;
    GParamSpec *mount_point_param;
    GParamSpec *device_path_param;
    GParamSpec *control_path_param;
    GParamSpec *device_model_param;
    GParamSpec *device_model_string_param;
    GParamSpec *device_name_param;
    GParamSpec *user_name_param;
    GParamSpec *host_name_param;
    GParamSpec *volume_size_param;
    GParamSpec *volume_available_param;
    GParamSpec *volume_used_param;
    GParamSpec *is_ipod_param;
    GParamSpec *is_new_param;
    GParamSpec *serial_number_param;
    GParamSpec *model_number_param;
    GParamSpec *firmware_version_param;
    GParamSpec *volume_uuid_param;
    GParamSpec *volume_label_param;
    GParamSpec *can_write_param;
    GParamSpec *device_generation_param;
    GParamSpec *advertised_capacity_param;
    GParamSpec *artwork_format_param;
    GParamSpec *manufacturer_id_param;
    GParamSpec *production_year_param;
    GParamSpec *production_week_param;
    GParamSpec *production_index_param;

    GObjectClass *class = G_OBJECT_CLASS(klass);

    parent_class = g_type_class_peek_parent(klass);
    class->finalize = ipod_device_finalize;
    
    hal_context_param = g_param_spec_pointer("hal-context", "HAL Context",
        "LibHalContext handle", G_PARAM_READABLE);
    
    hal_volume_id_param = g_param_spec_string("hal-volume-id", "HAL Volume ID", 
        "Volume ID of a device in HAL",
        NULL, G_PARAM_READWRITE);
    
    mount_point_param = g_param_spec_string("mount-point", "Mount Point", 
        "Where iPod is mounted (parent of an iPod_Control directory)",
        NULL, G_PARAM_READWRITE);
    
    device_path_param = g_param_spec_string("device-path", "Device Path",
        "Path to raw iPod Device (/dev/sda2, for example)",
        NULL, G_PARAM_READWRITE);
        
    control_path_param = g_param_spec_string("control-path", 
        "iPod_Control Path","Full path to iPod_Control", 
        NULL, G_PARAM_READABLE);
         
    device_model_param = g_param_spec_uint("device-model", "Device Model",
        "Type of iPod (Regular, Photo, Shuffle)", 0, 2, 0, G_PARAM_READABLE);
    
    device_model_string_param = g_param_spec_string("device-model-string",
        "Device Model String", "String type of iPod (Regular, Shuffle)",
        NULL, G_PARAM_READABLE);
    
    device_name_param = g_param_spec_string("device-name", "Device Name",
        "The user-assigned name of their iPod", NULL, G_PARAM_READWRITE);
         
    user_name_param = g_param_spec_string("user-name", "User Name",
        "On Windows, Maybe Mac, the user account owning the iPod",
        NULL, G_PARAM_READWRITE);
        
    host_name_param = g_param_spec_string("host-name", "Host Name",
        "On Windows, Maybe Mac, the host/computer name owning the iPod",
        NULL, G_PARAM_READWRITE);
        
    volume_size_param = g_param_spec_uint64("volume-size", "Volume Size",
        "Total size of the iPod's hard drive", 0, G_MAXLONG, 0, 
        G_PARAM_READABLE);
        
    volume_available_param = g_param_spec_uint64("volume-available", 
        "Volume Available", "Available space on the iPod",
        0, G_MAXLONG, 0, G_PARAM_READABLE);
        
    volume_used_param = g_param_spec_uint64("volume-used", "Volume Used",
        "How much space has been used", 0, G_MAXLONG, 0, G_PARAM_READABLE);
    
    is_ipod_param = g_param_spec_boolean("is-ipod", "Is iPod",
        "If all device checks are okay, then this is true",
        FALSE, G_PARAM_READABLE);
        
    is_new_param = g_param_spec_boolean("is-new", "Is iPod",
        "If device is fresh from factory/restore, this is true",
        FALSE, G_PARAM_READABLE);
    
    serial_number_param = g_param_spec_string("serial-number", "Serial Number",
        "Serial Number of the iPod",
        NULL, G_PARAM_READABLE);
        
    model_number_param = g_param_spec_string("model-number", "Model Number",
        "Model Number of the iPod",
        NULL, G_PARAM_READABLE);
        
    firmware_version_param = g_param_spec_string("firmware-version",
        "Firmware Version", "iPod Firmware Version",
        NULL, G_PARAM_READABLE);
            
    volume_uuid_param = g_param_spec_string("volume-uuid", "Volume UUID",
        "Volume UUID of the iPod",
        NULL, G_PARAM_READABLE);
        
    volume_label_param = g_param_spec_string("volume-label", "Volume Label",
        "Volume Label of the iPod",
        NULL, G_PARAM_READABLE);
        
    can_write_param = g_param_spec_boolean("can-write", "Can Write",
        "True if device can be written to (mounted read/write)",
        FALSE, G_PARAM_READABLE);

    advertised_capacity_param = g_param_spec_string("advertised-capacity", 
        "Advertised Capacity", "Apple Marketed/Advertised Capacity String",
        NULL, G_PARAM_READABLE);

    device_generation_param = g_param_spec_uint("device-generation", 
        "Generation", "Generation of the iPod",
        0, G_MAXUINT, 0, G_PARAM_READABLE);

    artwork_format_param = g_param_spec_pointer("artwork-formats",
        "Artwork Format", "Support Artwork Formats", G_PARAM_READABLE);
    
    manufacturer_id_param = g_param_spec_string("manufacturer-id", 
        "Manufacturer ID", "ID of the plant that manufactured this iPod",
        NULL, G_PARAM_READABLE);
        
    production_year_param = g_param_spec_uint("production-year", 
        "Production Year", "Year this iPod was produced",
        0, G_MAXUINT, 0, G_PARAM_READABLE);
        
    production_week_param = g_param_spec_uint("production-week", 
        "Production Week", "Week number of this iPod in its production year",
        0, G_MAXUINT, 0, G_PARAM_READABLE);
        
    production_index_param = g_param_spec_uint("production-index", 
        "Production Index", "Production index of this iPod in its production week",
        0, G_MAXUINT, 0, G_PARAM_READABLE);
    
    class->set_property = ipod_device_set_property;
    class->get_property = ipod_device_get_property;    
    
    g_object_class_install_property(class, PROP_HAL_CONTEXT,
        hal_context_param);
    
    g_object_class_install_property(class, PROP_HAL_VOLUME_ID,
        hal_volume_id_param);
    
    g_object_class_install_property(class, PROP_MOUNT_POINT, 
        mount_point_param);
    
    g_object_class_install_property(class, PROP_DEVICE_PATH, 
        device_path_param);
    
    g_object_class_install_property(class, PROP_CONTROL_PATH, 
        control_path_param);
    
    g_object_class_install_property(class, PROP_DEVICE_MODEL, 
        device_model_param);
        
    g_object_class_install_property(class, PROP_DEVICE_MODEL_STRING,
        device_model_string_param);
        
    g_object_class_install_property(class, PROP_DEVICE_NAME, 
        device_name_param);
        
    g_object_class_install_property(class, PROP_USER_NAME, 
        user_name_param);
        
    g_object_class_install_property(class, PROP_HOST_NAME, 
        host_name_param);
        
    g_object_class_install_property(class, PROP_VOLUME_SIZE, 
        volume_size_param);
        
    g_object_class_install_property(class, PROP_VOLUME_AVAILABLE, 
        volume_available_param);
        
    g_object_class_install_property(class, PROP_VOLUME_USED, 
        volume_used_param);
        
    g_object_class_install_property(class, PROP_IS_IPOD, is_ipod_param);
    
    g_object_class_install_property(class, PROP_IS_NEW, is_new_param);
    
    g_object_class_install_property(class, PROP_SERIAL_NUMBER, 
        serial_number_param);
    
    g_object_class_install_property(class, PROP_MODEL_NUMBER, 
        model_number_param);
    
    g_object_class_install_property(class, PROP_FIRMWARE_VERSION, 
        firmware_version_param);
    
    g_object_class_install_property(class, PROP_VOLUME_UUID, 
        volume_uuid_param);
    
    g_object_class_install_property(class, PROP_VOLUME_LABEL, 
        volume_label_param);

    g_object_class_install_property(class, PROP_CAN_WRITE,
        can_write_param);
        
    g_object_class_install_property(class, PROP_DEVICE_GENERATION, 
        device_generation_param);

    g_object_class_install_property(class, PROP_ADVERTISED_CAPACITY,
        advertised_capacity_param);
    
    g_object_class_install_property(class, PROP_ARTWORK_FORMAT,
        artwork_format_param);
    
    g_object_class_install_property(class, PROP_MANUFACTURER_ID,
        manufacturer_id_param);
        
    g_object_class_install_property(class, PROP_PRODUCTION_YEAR,
        production_year_param);
        
    g_object_class_install_property(class, PROP_PRODUCTION_WEEK,
        production_week_param);
        
    g_object_class_install_property(class, PROP_PRODUCTION_INDEX,
        production_index_param);
}

static void
ipod_device_init(IpodDevice *device)
{
    device->priv = g_new0(IpodDevicePrivate, 1);
    
    device->priv->hal_context = ipod_device_hal_initialize();
    
    device->priv->hal_volume_id = NULL;
    device->priv->mount_point = NULL;
    device->priv->device_path = NULL;
    device->priv->control_path = NULL;
    device->priv->device_name = NULL;
    device->priv->user_name = NULL;
    device->priv->host_name = NULL;
    device->priv->adv_capacity = NULL;
    device->priv->volume_uuid = NULL;
    device->priv->volume_label = NULL;
    
    device->priv->volume_size = 0;
    device->priv->volume_available = 0;
    device->priv->volume_used = 0;
    
    device->priv->is_new = FALSE;
    device->priv->can_write = FALSE;
    
    memset(&device->priv->serial_number, 0, sizeof(IpodSerialNumber));
}

static void
ipod_device_finalize(GObject *object)
{
    IpodDevice *device = IPOD_DEVICE(object);
    
    /* Free private members, etc. */
    g_free_if_not_null(device->priv->hal_volume_id);
    g_free_if_not_null(device->priv->device_path);
    g_free_if_not_null(device->priv->mount_point);
    g_free_if_not_null(device->priv->control_path);
    g_free_if_not_null(device->priv->device_name);
    g_free_if_not_null(device->priv->user_name);
    g_free_if_not_null(device->priv->host_name);
    g_free_if_not_null(device->priv->adv_capacity);
    g_free_if_not_null(device->priv->volume_uuid);
    g_free_if_not_null(device->priv->volume_label);
    g_free_if_not_null(device->priv->model_number);
    g_free_if_not_null(device->priv->serial_number.serial);
    g_free_if_not_null(device->priv->firmware_version);
    g_free_if_not_null(device->priv->artwork_formats);

    if(device->priv->hal_context != NULL) {
        libhal_ctx_shutdown(device->priv->hal_context, NULL);
        libhal_ctx_free(device->priv->hal_context);
    }
    
    g_free(device->priv);
    G_OBJECT_CLASS(parent_class)->finalize(object);
}

/* Private Methods */

static LibHalContext *
ipod_device_hal_initialize()
{
    LibHalContext *hal_context;
    DBusError error;
    DBusConnection *dbus_connection;
    char **devices;
    gint device_count;

    hal_context = libhal_ctx_new();
    if(hal_context == NULL) {
        return NULL;
    }
    
    dbus_error_init(&error);
    dbus_connection = dbus_bus_get(DBUS_BUS_SYSTEM, &error);
    if(dbus_error_is_set(&error)) {
        dbus_error_free(&error);
        libhal_ctx_free(hal_context);
        return NULL;
    }
    
    libhal_ctx_set_dbus_connection(hal_context, dbus_connection);
    
    if(!libhal_ctx_init(hal_context, &error)) {
        libhal_ctx_free(hal_context);
        return NULL;
    }

    devices = libhal_get_all_devices(hal_context, &device_count, NULL);
    if(devices == NULL) {
        libhal_ctx_shutdown(hal_context, NULL);
        libhal_ctx_free(hal_context);
        hal_context = NULL;
        return NULL;
    }
    
    libhal_free_string_array(devices);
    
    return hal_context;
}

static gboolean
ipod_device_reload(IpodDevice *device)
{
    g_return_val_if_fail(IS_IPOD_DEVICE(device), FALSE);
    
    device->priv->model_index = 0;

    if(!ipod_device_detect_volume_info(device)) {
        return FALSE;
    }
    
    ipod_device_construct_paths(device);
    
    device->priv->is_new = !ipod_device_info_load(device);

    ipod_device_detect_volume_used(device);
    ipod_device_detect_writeable(device);
    ipod_device_read_sysinfo(device);
    ipod_device_detect_model(device);
    ipod_device_parse_serial(device);
    
    return device->priv->model_index != 0;
}

static void 
ipod_device_construct_paths(IpodDevice *device)
{
    int len;
    
    g_return_if_fail(IS_IPOD_DEVICE(device));
    g_return_if_fail(device->priv->mount_point != NULL);
    
    len = strlen(device->priv->mount_point);
    if(device->priv->mount_point[len - 1] == '/') {
        device->priv->mount_point[len - 1] = '\0';
    }
    
    if(strlen(device->priv->mount_point) == 0) {
        return;
    }
    
    device->priv->control_path = g_strdup_printf("%s/%s",
        device->priv->mount_point, "iPod_Control/");
}

static gchar *
ipod_device_read_device_info_string(FILE *fd)
{
    gshort length;
    gunichar2 *utf16;
    gchar *utf8;
    
    if(fread(&length, 1, sizeof(gshort), fd) <= 0) {
        return NULL;
    } else if(length <= 0) {
        return NULL;
    }
    
    utf16 = (gunichar2 *)g_malloc(length * sizeof(gunichar2));
    if(utf16 == NULL) {
        return NULL;
    } else if(fread(utf16, sizeof(gunichar2), length, fd) <= 0) {
        return NULL;
    }
    
    utf8 = g_utf16_to_utf8(utf16, length, NULL, NULL, NULL);
    
    g_free(utf16);
    utf16 = NULL;
    
    return utf8;
}

static void
ipod_device_write_device_info_string(gchar *str, FILE *fd)
{
    gunichar2 *unistr;
    gshort length;

    if(str == NULL) {
        return;
    }
    
    length = strlen(str);
    unistr = g_utf8_to_utf16(str, length, NULL, NULL, NULL);
    
    length = length > 0x198 ? 0x198 : length;

    if(fwrite(&length, 2, 1, fd) <= 0 
        || fwrite(unistr, 2, length, fd) <= 0) {
        g_warning("Could not write to DeviceInfo file");
    }
    
    g_free(unistr);
}

static char *
ipod_device_get_extended_sysinfo_path(IpodDevice *device)
{
    DBusMessage *dmesg, *reply;
    DBusError error;
    GPtrArray *options;
    DBusConnection *dbus_connection;
    char *path;
    char *udi;

    if(!ipod_prepare_sysinfo (device->priv->serial_number.serial, 
        device->priv->can_write, device->priv->mount_point, &path)) {
        return NULL;
    }

    //g_debug("SysInfo PLIST XML: %s", path);

    if (g_file_test (path, G_FILE_TEST_EXISTS)) {
        return path;
    }
    
    udi = device->priv->hal_volume_id;
    
    dbus_error_init(&error);
    dbus_connection = dbus_bus_get(DBUS_BUS_SYSTEM, &error);
    
    if(dbus_error_is_set(&error)) {
        dbus_error_free(&error);
        g_free (path);
        return NULL;
    }

    
    if(!(dmesg = dbus_message_new_method_call("org.freedesktop.Hal", udi,
        "org.freedesktop.Hal.Device.Volume", "WriteIpodInfo"))) {
        g_free (path);
        return NULL;
    }
    
    options = g_ptr_array_new();

    if(!dbus_message_append_args(dmesg, 
          DBUS_TYPE_ARRAY, DBUS_TYPE_STRING, &options->pdata, options->len,
        DBUS_TYPE_INVALID)) {
        dbus_message_unref(dmesg);
        g_ptr_array_free(options, TRUE);
        g_free (path);
        return NULL;
    }

    g_ptr_array_free(options, TRUE);
    
    if(!(reply = dbus_connection_send_with_reply_and_block(
        dbus_connection, dmesg, -1, &error))) {
        dbus_message_unref(dmesg);
        dbus_error_free(&error);
        g_free (path);
        return NULL;
    }

    dbus_message_unref(dmesg);
    dbus_message_unref(reply);

    if (!g_file_test (path, G_FILE_TEST_EXISTS)) {
        g_free (path);
        return NULL;
    }
    
    return path;
}

static char *
ipod_device_get_dict_value (xmlXPathContextPtr xpath, const char *path, const char *text)
{
    xmlXPathObjectPtr xpath_item;
    char *pathstr;
    char *val = NULL;

    pathstr = g_strdup_printf ("%s/key[text()='%s']/following-sibling::*[1]", path, text);
    
    xpath_item = xmlXPathEvalExpression((xmlChar*)pathstr, xpath);
    if (xpath_item->nodesetval && xpath_item->nodesetval->nodeTab[0] != NULL) {
        val = (char *)xmlNodeGetContent (xpath_item->nodesetval->nodeTab[0]);
    }

    g_free (pathstr);
    xmlXPathFreeObject (xpath_item);
    return val;
}

static IpodPixelFormat
ipod_device_convert_pixel_format(const char *format)
{
    if (strcmp (format, "32767579") == 0) {
        return IPOD_PIXEL_FORMAT_IYUV;
    } else if (strcmp (format, "4C353635") == 0) {
        return IPOD_PIXEL_FORMAT_RGB565;
    } else if (strcmp (format, "42353635") == 0) {
    return IPOD_PIXEL_FORMAT_RGB565_BE;
    }
    
    return IPOD_PIXEL_FORMAT_UNKNOWN;
}

static void
ipod_device_parse_artwork_formats (GList **list, xmlXPathObjectPtr xpath_item, 
    IpodArtworkUsage usage)
{
    int i;
    
    g_return_if_fail(xpath_item != NULL);
    g_return_if_fail(xpath_item->nodesetval != NULL);
    
    for (i = 0; i < xpath_item->nodesetval->nodeNr; i++) {
        IpodArtworkFormat *format = g_new0(typeof (IpodArtworkFormat), 1);
        xmlNodePtr dict = xpath_item->nodesetval->nodeTab[i];
        xmlNodePtr child;
        char *key = NULL;

        format->usage = usage;

        for (child = dict->children; child; child = child->next) {
            xmlChar *value;
        
            if (child->type != XML_ELEMENT_NODE) {
                continue;
            }
            
            value = xmlNodeGetContent(child);
            
            if (strcmp ((char *)child->name, "key") == 0) {
                    g_free_if_not_null (key);
                key = g_strdup ((char *)value);
            } else if (strcmp(key, "FormatId") == 0) {
                format->correlation_id = atoi((char *)value);
            } else if (strcmp(key, "RenderWidth") == 0) {
                format->width = atoi((char *)value);
            } else if (strcmp(key, "RenderHeight") == 0) {
                format->height = atoi((char *)value);
            } else if (strcmp(key, "PixelFormat") == 0) {
                format->pixel_format = ipod_device_convert_pixel_format((char *)value);
            } else if (strcmp(key, "Rotation") == 0) {
                format->rotation = atoi((char *)value);
            }

            xmlFree (value);
        }

        /* FIXME: is this really always the case? */
        format->image_size = format->width * format->height * 2;

        g_free_if_not_null (key);

        *list = g_list_prepend (*list, format);
    }
}

static void
ipod_device_read_artwork_info (IpodDevice *device, xmlXPathContextPtr xpath)
{
    GList *list = NULL;
    GList *l;
    xmlXPathObjectPtr xpath_item;
    int i;
    IpodArtworkFormat last_format = { -1, -1, -1, -1, -1, -1, -1 };

    xpath_item = xmlXPathEvalExpression((xmlChar *)"/plist/dict/key[text()='ImageSpecifications']/following-sibling::*[1]//dict", xpath);
    if(xpath_item != NULL) {
        if(xpath_item->nodesetval != NULL) {
            ipod_device_parse_artwork_formats (&list, xpath_item, IPOD_ARTWORK_PHOTO);
        }
        xmlXPathFreeObject (xpath_item);
    }
    
    xpath_item = xmlXPathEvalExpression((xmlChar *)"/plist/dict/key[text()='AlbumArt']/following-sibling::*[1]//dict", xpath);
    if(xpath_item != NULL) {
        if(xpath_item->nodesetval != NULL) {
            ipod_device_parse_artwork_formats (&list, xpath_item, IPOD_ARTWORK_COVER);
        }
        xmlXPathFreeObject (xpath_item);
    }
    
    if (device->priv->artwork_formats != NULL) {
        g_free (device->priv->artwork_formats);
    }
    
    if(list != NULL && g_list_length(list) > 0) {
        device->priv->artwork_formats = g_new0(IpodArtworkFormat, g_list_length (list) + 1);

        i = 0;
        for (l = list; l; l = l->next) {
            device->priv->artwork_formats[i++] = *(IpodArtworkFormat *)l->data;
        }

        device->priv->artwork_formats[i] = last_format;
    } else {
        device->priv->artwork_formats = NULL;
    }
}

static gboolean
ipod_device_read_sysinfo_hal(IpodDevice *device)
{
    char *path;
    xmlDocPtr doc;
    xmlNodePtr root;
    xmlXPathContextPtr xpath;

    device->priv->serial_number.serial = 
        g_strstrip (libhal_device_get_property_string (device->priv->hal_context,
            device->priv->hal_volume_id,
            "info.ipod.serial_number", NULL));
    
    path = ipod_device_get_extended_sysinfo_path (device);
    if (path == NULL) {
        return FALSE;
    }

    doc = xmlParseFile (path);
    g_free (path);
    
    if (doc == NULL) {
        return FALSE;
    }
    
    root = xmlDocGetRootElement(doc);
    if(root == NULL) {
        xmlFreeDoc(doc);
        return FALSE;
    }

    xpath = xmlXPathNewContext(doc);

    device->priv->serial_number.serial = g_strstrip (ipod_device_get_dict_value (
        xpath, "/plist/dict", "SerialNumber"));

    ipod_device_read_artwork_info (device, xpath);

    xmlXPathFreeContext (xpath);
    xmlFreeDoc (doc);

    return TRUE;
}

static gboolean
ipod_device_read_sysinfo(IpodDevice *device)
{
    gchar *path, buf[512];
    gboolean corrupt_check = FALSE;
    gboolean hal_result;
    FILE *fd;
    
    g_return_val_if_fail(IS_IPOD_DEVICE(device), FALSE);

    hal_result = ipod_device_read_sysinfo_hal (device);

    path = g_strdup_printf("%sDevice/SysInfo", device->priv->control_path);
    
    fd = fopen(path, "r");
    if(fd == NULL) {
        g_free(path);
        return hal_result;
    }
    
    while(fgets(buf, sizeof(buf), fd)) {
        gint key_length, value_length;
        gchar *key, *value, *p;
        
        // there have been reports that SysInfo can become "corrupt";
        // the iPod writes the Play Counts file in place of SysInfo
        if(!corrupt_check && strncasecmp(buf, "mhdp", 4) == 0) {
            g_fprintf(stderr,
                "*******************************************************************\n"
                "Your iPod's SysInfo file is corrupt! To fix it, run the\n"
                "following command and eject your iPod:\n\n"
                "  rm -rf %sDevice/\n\n"
                "This will cause the iPod to reboot and restore its SysInfo file\n"
                "after you eject it. You WILL lose your iPod preferences (language\n"
                "setting, clicker setting, etc.), but not the database, music, \n"
                "or other data. Perform at your own risk!\n"
                "*******************************************************************\n\n",
                device->priv->control_path);
            break;
        }
        
        corrupt_check = TRUE;
        
        if((value = strchr(buf, ':')) == NULL) {
            continue;
        }
        
        value += 2;
        value_length = strlen(value);
        key_length = value - buf - 2;
        
        if(value_length <= 4 || key_length <= 0) {
            continue;
        }
        
        value[value_length - 1] = '\0';
        
        key = g_strndup(buf, key_length);
        if(key == NULL) {
            continue;
        }
        
        // if it's a hex value, be sure to strip out the version string if it exists
        if(value[0] == '0' && value[1] == 'x' && (p = strchr(value, ' ')) != NULL) {
            *p = '\0';
        }

        if (strcmp (key, "pszSerialNumber") == 0) {
            device->priv->serial_number.serial = g_strstrip (g_strdup (value));
        } else if (strcmp (key, "ModelNumStr") == 0) {
            device->priv->model_number = g_strdup (value);
        } else if (strcmp (key, "buildID") == 0) {
            device->priv->firmware_version = g_strdup (value);
        }

        g_free (key);
    }
    
    fclose(fd);
    g_free(path);
    
    return TRUE;
}

static gboolean
ipod_device_info_load(IpodDevice *device)
{
    gchar *path;
    FILE *fd;
    
    g_return_val_if_fail(IS_IPOD_DEVICE(device), FALSE);
    
    path = g_strdup_printf("%siTunes/DeviceInfo", 
        device->priv->control_path);
    
    fd = fopen(path, "r");
    if(fd == NULL) {
        g_free(path);
        return FALSE;
    }
    
    device->priv->device_name = ipod_device_read_device_info_string(fd);
    if(device->priv->device_name == NULL) {
        device->priv->device_name = g_strdup("iPod");
    }
    
    fseek(fd, 0x200, SEEK_SET);
    device->priv->user_name = ipod_device_read_device_info_string(fd);
    
    fseek(fd, 0x400, SEEK_SET);
    device->priv->host_name = ipod_device_read_device_info_string(fd);
    
    fclose(fd);
    g_free(path);

    return TRUE;
}

static gboolean
ipod_device_detect_writeable(IpodDevice *device)
{
    FILE *fp;
    gchar *itunes_dir, *music_dir, *itunesdb_path;
    struct stat finfo;
        
    g_return_val_if_fail(IS_IPOD_DEVICE(device), FALSE);
    
    device->priv->can_write = FALSE;

    if(libhal_device_property_exists(device->priv->hal_context, 
        device->priv->hal_volume_id, "volume.fstype", NULL)) {
        gchar *fstype = libhal_device_get_property_string(
            device->priv->hal_context, device->priv->hal_volume_id, 
            "volume.fstype", NULL);

        if(strcmp(fstype, "hfsplus") == 0) {
            g_free(fstype);
            return FALSE;
        }

        g_free(fstype);
    }
        
    if(libhal_device_property_exists(device->priv->hal_context, device->priv->hal_volume_id, 
        "volume.is_mounted_read_only", NULL)) {
        device->priv->can_write = !libhal_device_get_property_bool(
            device->priv->hal_context, device->priv->hal_volume_id,
            "volume.is_mounted_read_only", NULL);
        return device->priv->can_write;
    }

    itunes_dir = g_strdup_printf("%siTunes", device->priv->control_path);
    
    if(g_mkdir_with_parents(itunes_dir, 0755) == -1) {
        g_free(itunes_dir);
        return FALSE;
    }
    
    itunesdb_path = g_strdup_printf("%s/iTunesDB", itunes_dir);
    
    if((fp = fopen(itunesdb_path, "a+")) != NULL) {
        device->priv->can_write = TRUE;
        fclose(fp);
        
        memset(&finfo, 0, sizeof(finfo));
        if(stat(itunesdb_path, &finfo) == 0) {
            if(finfo.st_size == 0) {
                unlink(itunesdb_path);
            }
        }
    } else {
        g_free(itunes_dir);
        g_free(itunesdb_path);
        return FALSE;
    }

    music_dir = g_strdup_printf("%sMusic", device->priv->control_path);
    
    if(!g_file_test(music_dir, G_FILE_TEST_IS_DIR)) {
        device->priv->can_write = g_mkdir_with_parents(music_dir, 0755) == 0;
    }
    
    g_free(itunes_dir);
    g_free(itunesdb_path);
    g_free(music_dir);
    
    return device->priv->can_write;
}

static gint
ipod_device_get_model_index_from_table(const gchar *_model_num)
{
    gint i;
    gchar *model_num = g_strdup(_model_num);
    gchar *p = model_num;
    
    if(isalpha(model_num[0])) {
        p++;
    }
    
    for(i = 2; ipod_model_table[i].model_number != NULL; i++) {
        if(g_strncasecmp(p, ipod_model_table[i].model_number, 4) == 0) {
            g_free(model_num);
            return i;
        }
    }    
    
    g_free(model_num);
    return 1;
}

static guint
ipod_device_detect_model(IpodDevice *device)
{
    gint i;
    guint64 adv, act;
    gint cap;
    const gchar *model_number;
    const gchar *serial_number_suffix;
    const gchar *board_rev = NULL;
    
    g_return_val_if_fail(IS_IPOD_DEVICE(device), 0);

    /* We know we have an iPod ... we just don't know what it is */
       device->priv->model_index = 1;
    
    model_number = device->priv->model_number;
    serial_number_suffix = NULL;
    
    if (device->priv->serial_number.serial != NULL) {
        serial_number_suffix = &device->priv->serial_number.serial[strlen(device->priv->serial_number.serial) - 3];
    }
    
    if(model_number == NULL && serial_number_suffix != NULL) {
        guint64 current_size_diff = 0;
        
        /* probably have the 'extended' sysinfo which doesn't have model */
        /* we'll have to figure it out ourselves */
        
        for(i = 2; ipod_model_table[i].model_number != NULL; i++) {
            if(ipod_model_table[i].serial_numbers == NULL ||  
                strstr(ipod_model_table[i].serial_numbers, serial_number_suffix) == NULL) {
                continue;
            }

            cap = ipod_model_table[i].capacity;
            adv = cap * 1048576; /* convert to bytes */
            act = device->priv->volume_size;

            if (current_size_diff == 0 || abs(adv - act) < current_size_diff) {
                current_size_diff = abs(adv - act);
                device->priv->model_index = i;
                device->priv->model_number = g_strdup (ipod_model_table[i].model_number);
                device->priv->adv_capacity = CAPACITY_DISPLAY_STRING(cap);
            }
        }
    } else if(model_number == NULL) {
        /* Guess the model number based on capacity and generation;
           2G iPods and older do not have ModelNumStr in SysInfo */
           
        glong raw_generation = (glong)g_ascii_strtoull(board_rev, NULL, 16);
        gint i, generation = raw_generation >> 16;
        const gchar *model_num = NULL; 
        
        /* we have to guess based on generation and volume size :'( */
        if(generation == 1) {
            model_num = device->priv->volume_size >= 9000000000ll ?
                "8709" : "8513";
        } else if(generation == 2) {
            model_num = device->priv->volume_size >= 19000000000ll ?
                "8741" : "8737";
        }
        
        if(model_num != NULL) {
            device->priv->model_number = g_strdup_printf("M%s", model_num);
            
            for(i = 2; ipod_model_table[i].model_number != NULL; i++) {
                if(strcmp(ipod_model_table[i].model_number, model_num) == 0) {
                    cap = ipod_model_table[i].capacity;
                    device->priv->adv_capacity = CAPACITY_DISPLAY_STRING(cap);
                    device->priv->model_index = i;
                    break;
                }
            }
        }
    } else {
        /* Anything else */
        device->priv->model_index = ipod_device_get_model_index_from_table(model_number);
        cap = ipod_model_table[device->priv->model_index].capacity;
        device->priv->adv_capacity = CAPACITY_DISPLAY_STRING(cap);
    }

    return device->priv->model_index;
}

static void
ipod_device_parse_serial(IpodDevice *device)
{
    gchar *serial;
    gchar tmp_serial[9];
    IpodSerialNumber *decoded;
    
    g_return_if_fail(IS_IPOD_DEVICE(device));
    
    decoded = &device->priv->serial_number;
    g_return_if_fail(decoded != NULL);
    
    serial = decoded->serial;
    if(serial == NULL) {
        return;
    }
    
    g_return_if_fail(strlen(serial) == 11);
    
    memcpy(tmp_serial, serial, 8);
    memcpy(decoded->production_factory, serial, 2);
    
    tmp_serial[8] = 0;
    decoded->production_factory[2] = 0;
    
    decoded->production_year = (serial[2] - '0') + 2000;
    decoded->production_week = (serial[3] - '0') * 10 + (serial[4] - '0');
    
    memcpy(decoded->model_code, serial + 8, 3);
    decoded->model_code[3] = 0;
    
    decoded->production_number = strtol(tmp_serial + 5, NULL, 34);
    
    return;
}

static gboolean
ipod_device_detect_volume_info(IpodDevice *device)
{
    LibHalContext *hal_context;
    gchar **volumes;
    gchar *hd_mount_point, *hd_device_path;
    gchar *hd_hal_id = NULL, *maybe_hd_hal_id = NULL;
    gint volume_count, i;
    
    g_return_val_if_fail(IS_IPOD_DEVICE(device), FALSE);
    hal_context = device->priv->hal_context;
    
    g_free_if_not_null(device->priv->device_path);
    g_free_if_not_null(device->priv->mount_point);
    device->priv->volume_size = 0;
    
    if(!libhal_device_exists(hal_context, device->priv->hal_volume_id, NULL)) {
        /* For testing/debugging... we don't have a real device, but
           the location may be a directory containing an iPod image */
        if(g_strncasecmp(device->priv->hal_volume_id, "/dev/", 5) == 0 || 
            device->priv->hal_volume_id[0] != '/') {
            return FALSE;
        }
        
        g_free_if_not_null(device->priv->mount_point);
        device->priv->mount_point = g_strdup(device->priv->hal_volume_id);
        
        g_free_if_not_null(device->priv->hal_volume_id);
        g_free_if_not_null(device->priv->device_path);
        
        /* Let's find out about the disk drive containing our image */
    
        volumes = libhal_manager_find_device_string_match(hal_context, 
            "info.category", "volume", &volume_count, NULL);
        
        for(i = 0; i < volume_count; i++) {
            if(!libhal_device_property_exists(hal_context, 
                volumes[i], "volume.is_mounted", NULL) || 
                !libhal_device_get_property_bool(hal_context, 
                volumes[i], "volume.is_mounted", NULL)) {
                continue;
            }
            
            hd_mount_point = libhal_device_get_property_string(hal_context,
                volumes[i], "volume.mount_point", NULL);
            
            if(g_strncasecmp(hd_mount_point, device->priv->mount_point, 
                strlen(hd_mount_point)) != 0) {
                continue;
            }
            
            if(g_strcasecmp(hd_mount_point, "/") == 0) {
                maybe_hd_hal_id = volumes[i];
            } else { 
                hd_hal_id = volumes[i];
            }
        }
        
        if(hd_hal_id == NULL && maybe_hd_hal_id != NULL) {
            hd_hal_id = maybe_hd_hal_id;
        }
        
        if(hd_hal_id == NULL) {
            libhal_free_string_array(volumes);
            return FALSE;
        }
        
        if(!libhal_device_exists(hal_context, hd_hal_id, NULL)) {
            libhal_free_string_array(volumes);
            return FALSE;
        }
        
        hd_device_path = libhal_device_get_property_string(hal_context,
            hd_hal_id, "block.device", NULL);
        
        device->priv->hal_volume_id = g_strdup(hd_hal_id);
        device->priv->device_path = g_strdup(hd_device_path);
        device->priv->volume_size = libhal_device_get_property_uint64(
            hal_context, hd_hal_id, "volume.size", NULL);
        
        libhal_free_string_array(volumes);
        
        return TRUE;
    }
    
    if(!libhal_device_property_exists(hal_context, 
        device->priv->hal_volume_id, "volume.is_mounted", NULL) 
        || !libhal_device_get_property_bool(hal_context, 
        device->priv->hal_volume_id, "volume.is_mounted", NULL)) {
        return FALSE;
    }
            
    if(libhal_device_property_exists(hal_context, device->priv->hal_volume_id, 
        "block.device", NULL)) {
        device->priv->device_path = libhal_device_get_property_string(
            hal_context, device->priv->hal_volume_id, "block.device", NULL);
    }
    
    if(libhal_device_property_exists(hal_context, device->priv->hal_volume_id,
        "volume.mount_point", NULL)) {
        device->priv->mount_point = libhal_device_get_property_string(
            hal_context, device->priv->hal_volume_id, 
            "volume.mount_point", NULL);
    }

    if(libhal_device_property_exists(hal_context, device->priv->hal_volume_id,
        "volume.size", NULL)) {
        device->priv->volume_size = libhal_device_get_property_uint64(
            hal_context, device->priv->hal_volume_id, "volume.size", NULL);
    }
    
    if(libhal_device_property_exists(hal_context, device->priv->hal_volume_id,
        "volume.uuid", NULL)) {
        device->priv->volume_uuid = libhal_device_get_property_string(
            hal_context, device->priv->hal_volume_id,
            "volume.uuid", NULL);
        
        if(strlen(device->priv->volume_uuid) == 0) {
            g_free(device->priv->volume_uuid);
            device->priv->volume_uuid = NULL;
        }
    }

    if(libhal_device_property_exists(hal_context, device->priv->hal_volume_id,
        "volume.label", NULL)) {
        device->priv->volume_label = libhal_device_get_property_string(
            hal_context, device->priv->hal_volume_id,
            "volume.label", NULL);
    }
    
    return TRUE;
}

static void 
ipod_device_detect_volume_used(IpodDevice *device)
{
    glibtop_fsusage fsbuf;
    glibtop_get_fsusage(&fsbuf, device->priv->mount_point);

    device->priv->volume_available = fsbuf.bfree * fsbuf.block_size;
    device->priv->volume_used = device->priv->volume_size - 
        device->priv->volume_available;
}

/* Private Eject/Unmount Helper Methods */

#ifdef HAVE_HAL_EJECT

static gboolean
ipod_device_eject_hal(IpodDevice *device, gchar **error_out)
{
    DBusMessage *dmesg, *reply;
    DBusError error;
    GPtrArray *options;
    DBusConnection *dbus_connection;
    
    gchar *udi = device->priv->hal_volume_id;

    *error_out = NULL;

    dbus_error_init(&error);
    dbus_connection = dbus_bus_get(DBUS_BUS_SYSTEM, &error);
    if(dbus_error_is_set(&error)) {
        *error_out = g_strdup_printf("eject failed for %s: %s", udi, error.message);
        dbus_error_free(&error);
        return FALSE;
    }

    if(!(dmesg = dbus_message_new_method_call("org.freedesktop.Hal", udi,
        "org.freedesktop.Hal.Device.Volume", "Eject"))) {
        *error_out = g_strdup_printf("eject failed for %s: could not create dbus message", udi);
        return FALSE;
    }
    
    options = g_ptr_array_new();

    if(!dbus_message_append_args(dmesg, 
        DBUS_TYPE_ARRAY, DBUS_TYPE_STRING, &options->pdata, options->len,
        DBUS_TYPE_INVALID)) {
        *error_out = g_strdup_printf("eject failed for %s: could not append args to dbus message", udi);
        dbus_message_unref(dmesg);
        g_ptr_array_free(options, TRUE);
        return FALSE;
    }

    g_ptr_array_free(options, TRUE);
    
    if(!(reply = dbus_connection_send_with_reply_and_block(
        dbus_connection, dmesg, -1, &error))) {
        *error_out = g_strdup_printf("eject failed for %s: %s", udi, error.message);
        dbus_message_unref(dmesg);
        dbus_error_free(&error);
        return FALSE;
    }

    g_debug("eject queued for %s\n", udi);

    dbus_message_unref(dmesg);
    dbus_message_unref(reply);

    return TRUE;
}

#else

static gboolean 
ipod_device_has_open_fd(IpodDevice *device)
{
    GDir *dir, *fddir;
    gchar *fdpath, *fdidpath, *realpath;
    const gchar *procpath, *fdid;

    g_return_val_if_fail(IS_IPOD_DEVICE(device), FALSE);
    
    if((dir = g_dir_open("/proc", 0, NULL)) == NULL) {
        return FALSE;
    }
    
    while((procpath = g_dir_read_name(dir)) != NULL) {
        if(atoi(procpath) <= 0) {
            continue;
        }
        
        fdpath = g_strdup_printf("/proc/%s/fd", procpath);
        
        if(!g_file_test(fdpath, G_FILE_TEST_IS_DIR)) {
            g_free(fdpath);
            continue;
        }
        
        if((fddir = g_dir_open(fdpath, 0, NULL)) == NULL) {
            g_free(fdpath);
            continue;
        }
        
        while((fdid = g_dir_read_name(fddir)) != NULL) {
            fdidpath = g_strdup_printf("%s/%s", fdpath, fdid);
            realpath = g_file_read_link(fdidpath, NULL);
            
            if(realpath == NULL) {
                g_free(fdidpath);
                continue;
            }
            
            if(g_strncasecmp(realpath, device->priv->mount_point, 
                strlen(device->priv->mount_point)) == 0) {
                g_dir_close(fddir);
                g_dir_close(dir);
                g_free(realpath);
                g_free(fdidpath);
                return TRUE;
            }
            
            g_free(realpath);
            g_free(fdidpath);
        }
        
        g_dir_close(fddir);
    }
    
    g_dir_close(dir);
    
    return FALSE;
}

static gboolean 
ipod_device_udi_is_subfs_mount(IpodDevice *device)
{
    gint subfs = FALSE;
    gchar **callouts;
    gint i;
    
    if((callouts = libhal_device_get_property_strlist(device->priv->hal_context,
        device->priv->hal_volume_id, "info.callouts.add", NULL))) {
        for(i = 0; callouts[i] != NULL; i++) {
            if(!strcmp(callouts[i], "hald-subfs-mount")) {
                subfs = TRUE;
                g_debug("subfs handles mount for %s; skipping unmount", 
                    device->priv->device_path);
                break;
            }
        }
        
        libhal_free_string_array(callouts);
    }
    
    return subfs;
}

static int
ipod_device_run_command(IpodDevice *device, const char *command, 
    GError **error_out)
{
    char *path;
    const char *inptr, *start;
    GError *error = NULL;
    GString *exec;
    char *argv[4];
    int status = 0;
    
    exec = g_string_new(NULL);
    
    /* perform s/%d/device/, s/%m/mount_point/ and s/%h/udi/ */
    start = inptr = command;
    while((inptr = strchr (inptr, '%')) != NULL) {
        g_string_append_len(exec, start, inptr - start);
        inptr++;
        switch (*inptr) {
        case 'd':
            g_string_append(exec, device->priv->device_path ? 
                device->priv->device_path : "");
            break;
        case 'm':
            if(device->priv->mount_point) {
                path = g_shell_quote(device->priv->mount_point);
                g_string_append(exec, path);
                g_free(path);
            } else {
                g_string_append(exec, "\"\"");
            }
            break;
        case 'h':
            g_string_append(exec, device->priv->hal_volume_id);
            break;
        case '%':
            g_string_append_c(exec, '%');
            break;
        default:
            g_string_append_c(exec, '%');
            if(*inptr) {
                g_string_append_c(exec, *inptr);
            }
            break;
        }
        
        if(*inptr) {
            inptr++;
        }
        
        start = inptr;
    }
    
    g_string_append(exec, start);
    
    argv[0] = "/bin/sh";
    argv[1] = "-c";
    argv[2] = exec->str;
    argv[3] = NULL;

    g_spawn_sync(g_get_home_dir(), argv, NULL, 0, NULL, 
        NULL, NULL, NULL, &status, &error);
    
    if(error != NULL) {
        g_propagate_error(error_out, error);
    }
    
    g_string_free(exec, TRUE);
    
    return status;
}

#endif

/* Public Methods */

IpodDevice *
ipod_device_new(const gchar *hal_volume_id)
{
    IpodDevice *device = g_object_new(TYPE_IPOD_DEVICE, 
        "hal-volume-id", hal_volume_id, NULL);
        
    if(device == NULL) {
        return NULL;
    }

    if(!device->priv->is_ipod) {
        g_object_unref(device);
        return NULL;
    }
    
    return device;
}

gboolean 
ipod_device_rescan_disk(IpodDevice *device)
{
    g_return_val_if_fail(IS_IPOD_DEVICE(device), FALSE);
    ipod_device_detect_volume_used(device);
    return TRUE;
}

guint
ipod_device_eject(IpodDevice *device, GError **error_out)
{
    GError *error = NULL;
#ifdef HAVE_HAL_EJECT
    gchar *hal_eject_error = NULL;
#else
    gint exit_status;
    gchar *alt_command;
#endif

    g_return_val_if_fail(IS_IPOD_DEVICE(device), EJECT_ERROR);
    g_return_val_if_fail(device->priv->is_ipod, EJECT_ERROR);
    
#ifdef HAVE_HAL_EJECT

    if(ipod_device_eject_hal(device, &hal_eject_error) == TRUE) {
        return EJECT_OK;
    } else {
        error = g_error_new(g_quark_from_static_string("IPOD_DEVICE"),
            ERROR_EJECT, hal_eject_error);
        g_propagate_error(error_out, error);
    }
    
    return EJECT_ERROR;
    
#else

    if(ipod_device_udi_is_subfs_mount(device)) {
        if(ipod_device_has_open_fd(device)) {
            return EJECT_BUSY;
        }

        sync();
    
        exit_status = ipod_device_run_command(device, EJECT_COMMAND, &error);
        if(error) {
            g_propagate_error(error_out, error);
            return EJECT_ERROR;
        }
        
        return exit_status == 0 ? EJECT_OK : EJECT_ERROR;
    }
        
    exit_status = ipod_device_run_command(device, UNMOUNT_COMMAND, &error);
    if(exit_status != 0 || error != NULL) {
        g_warning("Could not execute unmount command libipoddevice was "
            "configured with, falling back on generic umount");
        alt_command = g_strdup_printf("umount %s", device->priv->device_path);
        exit_status = ipod_device_run_command(device, alt_command, &error);
        g_free(alt_command);
    }
    
    if(error == NULL && exit_status == 0) {
        exit_status = ipod_device_run_command(device, EJECT_COMMAND, &error);
        if(error != NULL) {
            g_propagate_error(error_out, error);
            return EJECT_ERROR;
        }
        
        return exit_status == 0 ? EJECT_OK : EJECT_ERROR;
    } 
    
    if(error) {
        g_propagate_error(error_out, error);
    }
    
    return EJECT_ERROR;
#endif
}

static GList *
_ipod_device_list_devices(gboolean create_device)
{
    LibHalContext *hal_context;
    GList *finalDevices = NULL;
    gchar **ipods, **volumes;
    gint ipod_count, volume_count, i, j;
    IpodDevice *ipod;
    gboolean validIpod = FALSE;
    
    hal_context = ipod_device_hal_initialize();
    if(hal_context == NULL) {
        return NULL;
    }
    
    ipods = libhal_manager_find_device_string_match(hal_context, 
        "info.product", "iPod", &ipod_count, NULL);
    
    for(i = 0; i < ipod_count; i++) {
        volumes = libhal_manager_find_device_string_match(hal_context,
            "info.parent", ipods[i], &volume_count, NULL);
        
        for(j = 0; j < volume_count; j++) {
            if(!libhal_device_property_exists(hal_context, 
                volumes[j], "volume.is_mounted", NULL) 
                || !libhal_device_get_property_bool(hal_context, 
                volumes[j], "volume.is_mounted", NULL)) {
                continue;
            }
            
            if(!create_device) {
                finalDevices = g_list_append(finalDevices, 
                    g_strdup(volumes[j]));
                continue;
            }
                
            if((ipod = ipod_device_new(volumes[j])) == NULL) {
                continue;
            }
            
            g_object_get(ipod, "is-ipod", &validIpod, NULL);
            if(validIpod) {
                finalDevices = g_list_append(finalDevices, ipod);
            }
        }
    }
    
    libhal_ctx_shutdown(hal_context, NULL);
    libhal_ctx_free(hal_context);
    
    return finalDevices;
}

GList *
ipod_device_list_devices()
{
    return _ipod_device_list_devices(TRUE);
}

GList *
ipod_device_list_device_udis()
{
    return _ipod_device_list_devices(FALSE);
}
    
gboolean
ipod_device_save(IpodDevice *device, GError **error_out)
{
    FILE *fd;
    gchar *path, *itunes_dir;
    gchar bs = 0;
    GError *error = NULL;
    
    g_return_val_if_fail(IS_IPOD_DEVICE(device), FALSE);
    
    itunes_dir = g_strdup_printf("%siTunes", device->priv->control_path);
    path = g_strdup_printf("%s/DeviceInfo", itunes_dir);
    
    if(g_mkdir_with_parents(itunes_dir, 0744) == -1) {
        if(error_out != NULL) {
            error = g_error_new(g_quark_from_static_string("IPOD_DEVICE"),
                ERROR_SAVE, "Could not create iTunes Directory: %s", 
                itunes_dir);
            g_propagate_error(error_out, error);
        }
        
        g_free(path);
        g_free(itunes_dir);
        
        return FALSE;
    }
    
    fd = fopen(path, "w+");
    if(fd == NULL) {
        if(error_out != NULL) {
            error = g_error_new(g_quark_from_static_string("IPOD_DEVICE"),
                ERROR_SAVE, "Could not save DeviceInfo file: %s", path);
            g_propagate_error(error_out, error);
        }
        
        g_free(path);
        g_free(itunes_dir);
        
        return FALSE;
    }
    
    ipod_device_write_device_info_string(device->priv->device_name, fd);
    
    fseek(fd, 0x200, SEEK_SET);
    ipod_device_write_device_info_string(device->priv->user_name, fd);
    
    fseek(fd, 0x400, SEEK_SET);
    ipod_device_write_device_info_string(device->priv->host_name, fd);
    
    fseek(fd, 0X5FF, SEEK_SET);
    if(fwrite(&bs, 1, 1, fd) <= 0) {
        g_warning("Could not write to DeviceInfo file");
    }
    
    fclose(fd);
    
    g_free(path);
    g_free(itunes_dir);
    
    return TRUE;
}

void
ipod_device_debug(IpodDevice *device)
{    
    static const gchar *generation_names [] = {
        "Unknown",
        "First (1)",
        "Second (2)",
        "Third (3)",
        "Fourth (4)",
        "Fifth (5)"
    };
    
    static const gchar *artwork_names [] = {
        "Photo:           ",
        "Cover:           "
    };

    gchar *device_path, *mount_point, *control_path, *hal_id;
    gchar *model_number, *adv_capacity, *model_string, *production_factory;
    guint model, generation, production_year, production_week, production_index;
    gboolean is_new, can_write;
    gchar *serial_number, *firmware_version;
    guint64 volume_size, volume_used, volume_available;
    gchar *volume_uuid, *volume_label;
    gchar *device_name, *user_name, *host_name;
    IpodArtworkFormat *artwork_formats;

    g_return_if_fail(IS_IPOD_DEVICE(device));
    
    g_object_get(device, 
        "device-path", &device_path,
        "mount-point", &mount_point,
        "control-path", &control_path,
        "hal-volume-id", &hal_id,
        "model-number", &model_number,
        "device-model", &model,
        "device-model-string", &model_string,
        "device-generation", &generation,
        "advertised-capacity", &adv_capacity,
        "is-new", &is_new,
        "can-write", &can_write,
        "serial-number", &serial_number,
        "firmware-version", &firmware_version,
        "volume-size", &volume_size,
        "volume-used", &volume_used,
        "volume-available", &volume_available,
        "volume_uuid", &volume_uuid,
        "volume-label", &volume_label,
        "device-name", &device_name,
        "user-name", &user_name,
        "host-name", &host_name,
        "artwork-formats", &artwork_formats,
        "manufacturer-id", &production_factory,
        "production-year", &production_year,
        "production-week", &production_week,
        "production-index", &production_index,
        NULL);
        
    g_printf("Path Info\n");
    g_printf("   Device Path:      %s\n", device_path);
    g_printf("   Mount Point:      %s\n", mount_point);
    g_printf("   Control Path:     %s\n", control_path);
    g_printf("   HAL ID:           %s\n", hal_id);
    
    g_printf("Device Info\n");
    g_printf("   Model Number:     %s\n", model_number);
    g_printf("   Device Model:     %s\n", model_string);
    g_printf("   iPod Generation:  %s\n", generation_names[generation]);
    g_printf("   Adv. Capacity:    %s\n", adv_capacity);
    g_printf("   Is New:           %s\n", is_new ? "YES" : "NO");
    g_printf("   Writable:         %s\n", can_write ? "YES" : "NO");
    g_printf("   Serial Number:    %s\n", serial_number);
    g_printf("   Firmware Version: %s\n", firmware_version);
    g_printf("   Manufacturer ID:  %s\n", production_factory);
    g_printf("   Production Year:  %04d\n", production_year);
    g_printf("   Production Week:  %02d\n", production_week);
    g_printf("   Production Index: %d\n", production_index);
    
    g_printf("Volume Info\n");
    g_printf("   Volume Size:      %"G_GUINT64_FORMAT"\n", volume_size);
    g_printf("   Volume Used:      %"G_GUINT64_FORMAT"\n", volume_used);
    g_printf("   Available         %"G_GUINT64_FORMAT"\n", volume_available);
    g_printf("   UUID:             %s\n", volume_uuid);
    g_printf("   Label             %s\n", volume_label);
    
    g_printf("User-Provided Info\n");
    g_printf("   Device Name:      %s\n", device_name);
    g_printf("   User Name:        %s\n", user_name);
    g_printf("   Host Name:        %s\n", host_name);

    if(artwork_formats != NULL) {
        IpodArtworkFormat *format;

        g_print("Supported Artwork Formats\n");
        format = artwork_formats;
        while(format->usage != -1) {
            printf("   %s %dx%d\n", artwork_names[format->usage],
                format->width, format->height);
            format++;
        }
    }

    g_printf("\n");
    fflush(stdout);
}

void ipod_device_dump_model_table_as_xml()
{
    gint i;
    
    g_printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    g_printf("<ipod-models>\n");
    for(i = 2; ipod_model_table[i].model_number != NULL; i++) {
        g_printf("<model>\n");
        g_printf("  <number>%s</number>\n", ipod_model_table[i].model_number); 
        g_printf("  <name>%s</name>\n", ipod_model_name_table[ipod_model_table[i].model_type]);
        g_printf("  <capacity>%"G_GUINT64_FORMAT"</capacity>\n", ipod_model_table[i].capacity);
        g_printf("  <generation>%d</generation>\n", ipod_model_table[i].generation);
        if(ipod_model_table[i].serial_numbers != NULL) {
            g_printf("  <serials>%s</serials>\n", ipod_model_table[i].serial_numbers);
        }
        g_printf("</model>\n");
    }
    g_printf("</ipod-models>\n");
}
