/***************************************************************************
*  glib-compat.h
*  Backported code from GLib
****************************************************************************/

/*  
 *  Copyright 2000 Red Hat, Inc.
 *
 * GLib is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * GLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GLib; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *   Boston, MA 02111-1307, USA.
 */

#ifndef _GLIB_COMPAT_H
#define _GLIB_COMPAT_H

#if !GLIB_CHECK_VERSION(2,8,0)
int g_mkdir_with_parents(const gchar *pathname, int mode);
#endif

#endif /* _GLIB_COMPAT_H */
