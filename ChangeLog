2007-04-19  Aaron Bockover  <abock@gnome.org>

	* src/ipod-model-table.h: Added XQX for 2nd gen blue shuffle, 1GB

2007-04-05  Aaron Bockover  <abock@gnome.org>

	* src/ipod-device.c: Do not require volume.is_mounted_read_only as we
	really need to support HAL < 0.5.8, which apparently is when that property
	was introduced. Check for HFS+, then check for volume.is_mounted_read_only
	and use it if exists, and then do legacy read/write tests

2007-04-01  Aaron Bockover  <abock@gnome.org>

	0.5.3 Released

	* NEWS: Updated

	* configure.ac: Bump to 0.5.3

2007-03-30  Aaron Bockover  <abock@gnome.org>

	* src/ipod-model-table.h: Use proper model types for the new shuffles

	* src/ipod-device.c: Detect if the volume is HFS+ explicitly and always
	return that the volume is read only

	* src/ipod-device.h: Added specific model types for the various kinds of
	second generation shuffles

2007-03-26  Aaron Bockover  <abock@gnome.org>

	Support HFS+ iPods a little better by being able to write/read the plist
	metadata off the device (in /tmp); Support 2nd generation iPod Shuffles

	* src/ipod-prepare-sysinfo.c: Build the extended sysinfo path (plist)
	based on if the device is writable or not

	* src/ipod-model-table.h: Added more serial codes to the model table
	and added support for second generation shuffles

	* src/hal-ipod-info.c: Write the plist to either /tmp/ipod-sysinfo-<serial>
	if the device is read only or to the correct place on the device otherwise

	* src/ipod-device.c: Read plist from above location; trim the serial number
	for whitespace as some plist data has trailing space

2006-12-09  Aaron Bockover  <abock@gnome.org>

	* src/ipod-device.h:
	* src/ipod-model-table.h: Added MODEL_TYPE_NANO_PRODUCT_RED
	for the iPod Nano (PRODUCT) RED iPod

2006-12-09  Aaron Bockover  <abock@gnome.org>

	0.5.2 Released

	* NEWS: Updated

	* configure.ac: bump to 0.5.1

	* src/ipod-model-table.h: Merged new model data from the submission
	table (http://banshee-project.org/IpodDataSubmit/Table)

2006-12-04  Aaron Bockover  <abock@gnome.org>

	* src/hal-ipod-info.c: Handle return value from asprintf (BGO #382224)

2006-10-12  Aaron Bockover  <abock@gnome.org>

	0.5.1 Released

	* NEWS:
	* AUTHORS: Updated

2006-10-12  Aaron Bockover  <abock@gnome.org>

	* configure.ac: Bump to 0.5.1 

	* src/ipod-model-table.h: Added new Nanos

2006-10-12  James Willcox  <snorp@snorp.net>

	* src/hal-ipod-info.c: get the serial number and put it in the
	info.ipod.serial_number property.

	* src/ipod-device.c: use info.ipod.serial_number instead of the
	value obtained from the xml (which might not exist).

	* data/20-ipod-info.fdi: add the hal-ipod-info callout

2006-10-01  Aaron Bockover  <abock@gnome.org>

	* src/Makefile.am:
	* src/ipod-device.c: 
	* src/ipod-model-table.h: Moved the model table to its own header file,
	added black 80GB video, V9P and fixed model number for G2 2GB silver nano

2006-09-26  James Willcox  <snorp@snorp.net>

	* data/20-ipod-info.fdi: match against volume.fsusage instead of
	volume.policy.should_mount

2006-09-19  Aaron Bockover  <abock@gnome.org>

	* src/ipod-device.c: Added support for new devices (silver 2GB nano - VQ5,
	and black 80GB video, V9S)

	* src/ipod-device.h: Added new nano model types 

2006-09-19  Aaron Bockover  <abock@gnome.org>

	* src/Makefile.am: hal-ipod-info into libdir/hal, not prefix/lib/hal

2006-09-19  Aaron Bockover  <abock@gnome.org>

	* src/ipod-device.c: Added serial code for 1GB fisrt-gen shuffle

2006-09-18  Aaron Bockover  <abock@gnome.org>

	* src/ipod-device.h:
	* src/ipod-device.c: Added ipod_device_dump_model_table_as_xml

	* src/ipod-device-test.c: Map --model-xml to 
	ipod_device_dump_model_table_as_xml

2006-09-18  James Willcox  <snorp@snorp.net>

	* src/ipod-device.c: correctly detect RGB565_BE pixel format

2006-09-18  Aaron Bockover  <abock@gnome.org>

	Work by James Willcox <snorp@snorp.net>:

	* src/hal-ipod-info.c: Use SG_IO to dump hidden XML plist document from
	iPod to the data partition<mount>/iPod_Control/Device/SysInfoExtended; 
	is a standalone program to be called by HAL; implementation of the 
	HAL method WriteIpodInfo, see data/20-ipod-info.fdi

	* src/ipod-device.c: Call WriteIpodInfo HAL method to try to generate
	SysInfoExtended and process it if it exists to get the serial number
	and artwork format info from the iPod, which is now required
	with iTunes 7, the latest iPod firmware, and new iPods as SysInfo
	is now obsolete
	
	SysInfo is still supported, the serial number->model number translation
	still has holes in it due to a lack of data

	XML DOM and XPath is used to read the serial number from SysInfoExtended
	and also if the device supports artwork, the information is read from
	this plist document as well (no longer do we need the static artwork
	tables)

	* data/20-ipod-info.fdi: FDI file to merge WriteIpodInfo/hal-ipod-info
	as a method on the mounted iPod data volume

	* data/Makefile.am: Added to build, install the FDI file

	* configure.ac: require libsgutils and libxml-2.0

	Work by Aaron Bockover <abock@gnome.org>

	* configure.ac: Bump to 0.5.0

	* ipod-device.c: Parse the iPod serial number into its constituent 
	components and make them available as properties (manufacturer ID,
	production year, production week, production index)

	Properly support unknown iPod models - even if a serial number or
	SysInfo file can't properly identify an iPod in our table, it should
	still be available as an unknown iPod to applications

2006-09-02  Aaron Bockover  <abock@gnome.org>

	* src/ipod-device-event-listener.c: Fixed use of g_list_remove exposed
	by gcc 4.1.2

2006-08-30  Aaron Bockover  <abock@gnome.org>

	* configure.ac: Remove pointless check for g_mkdir_with_parents,
	require GLib 2.6.0

	* src/glib-compat.[ch]: New files; added g_mkdir_with_parents copied
	from GLib 2.11.2 to be used if GLib version is less than 2.8.0

	* src/ipod-device.c: Include glib-compat.h; fixed minor pointer bug

	* src/Makefile.am: Added glib-compat.[ch]; build with -Werror

2006-08-05  Aaron Bockover  <abock@gnome.org>

	* configure.ac: Add missing opening [ on AC_ARG_WITH (Christoph Laimburg)

2006-07-06  Aaron Bockover  <aaron@abock.org>

	* configure.ac: Bump to 0.4.6 to make ipod-sharp 0.6.0 build fail if 
	libipoddevice 0.4.6 is not available; not released yet

2006-07-06  Aaron Bockover  <aaron@abock.org>

	* AUTHORS: Updated

	* libipoddevice.prj: Removed old Anjuta project file

2006-06-29  James Willcox  <snorp@snorp.net>

	* src/ipod-device.h: add IpodImageType enum for the various image
	formats used in thumbnails, and add 'image_type' and 'size' members
	to the IpodArtworkFormat struct.

	* src/ipod-device.c: fill in the new IpodArtworkFormat struct members.

2006-06-27  James Willcox  <snorp@snorp.net>

	* src/ipod-device.c: add model info for 1gb nanos

2006-04-13  Aaron Bockover  <aaron@abock.org>

	0.4.5 Released

	* configure.ac: Bump to 0.4.5

	* src/ipod-device.c: Fix critical segfault found on Shuffle iPods due
	to strtol not handling NULL input; use the safer g_ascii_strtoull 
	instead of strtol and explicitly check for NULL 

2006-04-11  Aaron Bockover  <aaron@abock.org>

	0.4.4 Released

	* configure.ac: Bump to 0.4.4

	* src/ipod-device.c: Handle proper return from fread; Add "Fifth" string
	to generation string table

2006-04-11  Aaron Bockover  <aaron@abock.org>

	0.4.3 Released

	* configure.ac: Bump to 0.4.3

2006-04-11  Aaron Bockover  <aaron@abock.org>

	* src/ipod-device.c: Use buildID instead of visibleBuildID from SysInfo
	to report firmware version; added a hex version parser to construct 
	a pretty version string

2006-04-10  Aaron Bockover  <aaron@abock.org>

	* src/ipod-device.c: Guess the model number based on iPod generation
	and volume size for 1G/2G iPods (BGO #338006)

2006-04-10  Aaron Bockover  <aaron@abock.org>

	0.4.2 Released

	* cconfigure.ac: Bump to 0.4.2; do not show unmount/eject summary if
	using HAL for ejecting

2006-04-10  Aaron Bockover  <aaron@abock.org>

	* src/ipod-device-test.c: Removed reboot support

	* src/ipod-device.c: Lots of clean up, reorganized some things; make
	gcc 4.1 happy by handling return values on some previously ignored
	functions (fread/fwrite); removed reboot support (never worked properly)

2006-04-10  Aaron Bockover  <aaron@abock.org>

	* src/ipod-device.c: Use a GData structure to hold full SysInfo table
	in memory for the lifetime of the instance; ipod_device_read_sysinfo 
	now has a faster and safer SysInfo parser and warns if SysInfo looks
	overwritten by Play Counts (apparently this is happening on some iPods)

2006-04-07  Aaron Bockover  <aaron@abock.org>

	* src/ipod-device.c: Check for an mhdp binary header in the SysInfo file;
	do not attempt to parse it if found... new format needs reverse 
	engineering now :/

2006-02-13  Aaron Bockover  <aaron@abock.org>

	0.4.1 Released

	* src/ipod-device.c (ipod_device_eject_hal): Propagate any errors to
	parent function, which will be sent to consumer application for display

	(ipod_device_eject): Handle the error_out parameter and propagate as
	a GError to caller
	
	* src/ipod-device.h: Added ERROR_EJECT

2006-02-13  Aaron Bockover  <aaron@abock.org>

	* src/ipod-device.c: Fixed minor formatting issue

2006-02-13  Aaron Bockover  <aaron@abock.org>

	* configure.ac: Bump to 0.4.1; check for HAL 0.5.6 to define 
	HAVE_HAL_EJECT, which is used for conditionally compiling DBus/HAL
	Method support for Ejecting over HAL

	* src/ipod-device.c (ipod_device_eject_hal): Method that calls a 
	DBus method against HAL to queue an eject request; needs HAL 0.5.6 or
	better

	(ipod_device_eject): Call ipod_device_eject_hal if HAVE_HAL_EJECT is 
	defined; ejecting centrally through HAL is *sooo* much better than
	execing some configured unmount/eject command sequence; this obsoletes
	umount and eject (directly), submount, and pmount, though all are still
	supported fallback options

2005-11-23  Aaron Bockover  <aaron@aaronbock.net>

	* configure.ac: Version 0.4.0

2005-11-23  Aaron Bockover  <aaron@aaronbock.net>

	* configure.ac: Remove all submount references/ASSUME_SUBMOUNT; now handled
	at runtime through HAL (info.callouts.add strlist)

	* src/ipod-device.c (ipod_device_udi_is_subfs_mount): Use HAL to determine
	if ipod volume mount is handled by submount; replaces ASSUME_SUBMOUNT
	define; fall back on a generic 'umount <device-node>' command for 
	unmounting if the configured unmount command fails (for instance,
	libipoddevice was configured with pumount, but pumount doesn't exist)

2005-11-21  Christophe Fergeau  <teuf@gnome.org>

	* src/ipod-device.c: mark some internal functions as static

2005-11-19  Miguel de Icaza  <miguel@novell.com>

	* configure.ac: Probe for g_mkdir_with_parents

	* src/ipod-device.c: Make this build on old systems by bundling
	g_mkdir_with_parents if needed.

2005-11-08  Aaron Bockover  <aaron@aaronbock.net>

	* src/hal-common.[ch]:
	* src/ipod-device-event-listener.[ch]:
	* src/ipod-device.[ch]:
	* src/ipod-device-test.c: Reformatted code; tabs->4 spaces, minor 
	other cosmetic code changes
	* configure.ac: Version 0.3.5

2005-11-07  Christophe Fergeau  <teuf@gnome.org>

	* src/hal-common.c:
	* src/hal-common.h: moved static variable from header to C file
	* src/ipod-device-test.c: include missing hal-common.h, get rid of
	#ifdef DEVBUILD since it doesn't seem to be used/defined anywhere else
	* src/ipod-device.c: (ipod_device_new):
	* src/ipod-device.h: marked ipod_device_new argument as const, and add
	missing G_END_DECLS
	* src/ipod-device.c: (ipod_device_new): don't crash if g_type_init
	hasn't been called before using libipoddevice

2005-11-05  Christophe Fergeau  <teuf@gnome.org>

	* src/ipod-device.c: (ipod_device_debug): added information about
	supported artwork formats to the debug ipod description

2005-11-02  Christophe Fergeau  <teuf@gnome.org>

	* src/ipod-device.c: (ipod_device_get_property),
	(ipod_device_class_init):
	* src/ipod-device.h: added an "artwork-formats" property to IpodDevice
	which returns a static IpodArtworkFormat array (terminated by an entry 
	filled with -1)

2005-10-18  Aaron Bockover  <aaron@aaronbock.net>

	* configure.ac: Require libgtop-2.0 to get disk usage information, much
	faster than previous method
	* ipod-device.c: ditto, using glibtop_get_fsusage(), removed 
	ipod_device_dir_size()

2005-10-17  Christophe Fergeau  <teuf@gnome.org>

	* src/ipod-device.c: (ipod_device_debug): Use printf format specifiers
	defined by glib to print guint64, this gets rid of warnings on amd64
	machines (and probably other 64 bit machines).

2005-10-21  Aaron Bockover  <aaron@aaronbock.net>

	* Makefile.am: got rid of EXTRADIST

	* configure.ac: Version 0.3.4
	* src/ipod-device.c: Added iPod Video model information to detection table
	* src/ipod-device.h: Added IPOD_TYPE_VIDEO_WHITE, IPOD_TYPE_VIDEO_BLACK
	and FIFTH_GENERATION enums

2005-09-21  Aaron Bockover  <aaron@aaronbock.net>
	
	* configure.ac: Version 0.3.3

2005-09-08  Aaron Bockover  <aaron@aaronbock.net>

	* src/ipod-device.c: Added iPod Nano model information to detection table
	and new GObject property for getting a string version of the model type
	name (i.e. MODEL_TYPE_NANO_WHITE -> "Nano (White)")
	* src/ipod-device.h: Added MODEL_TYPE_NANO_WHITE and MODEL_TYPE_NANO_BLACK,
	also PROP_DEVICE_MODEL_STRING

2005-09-07  Aaron Bockover  <aaron@aaronbock.net>

	* configure.ac: Version 0.3.2
	* src/ipod-device.c: Make the "control-path" property return the right
	internal value, use g_mkdir_with_parents for all directory creation

2005-09-05  JP Rosevear  <jpr@novell.com>

	* src/ipod-device.c (ipod_device_save): Create necessary parent
	directories as well for new ipods

2005-09-03  Aaron Bockover  <aaron@aaronbock.net>

	* src/ipod-device.c: Added HP E436 series iPod models to table

2005-08-24  Aaron Bockover  <aaron@aaronbock.net>

	* src/ipod-device.c: Fixed some minor bugs, backup device Prefs and
	restore on reboot (doesn't actually work, stupid iPod), fixed advertised
	capacity for shuffle

	* libipoddevice: Version 0.3.1
	* src/ipod-device-test.c: --reboot <device> option
	* src/ipod-device.c: Do not accept iPods without iPod_Control/Device,
	as this structure is created on reboot (for non-shuffles)
	* src/ipod-device.c: (ipod_device_reboot): like ipod_device_eject, but
	it reboots the iPod after ejecting
	* src/ipod-device.c: Critical bug fix! iTunesDB was being opened with w+
	which would destroy an existing copy... changed to a+

2005-08-23  Aaron Bockover  <aaron@aaronbock.net>

	* libipoddevice: Version 0.3.0
	* src/ipod-device.c: Model information table used to detect model type,
	generation, and advertised capacity based on model number. Removed
	file system tests to guess model type. Three new properties: can-write,
	device-generation, and advertised-capacity. Added more specific model 
	types. Using 'Color' instead of 'Photo' Debug now uses g_object_get
	to test properties instead of debugging directly on internal struct.
	* src/ipod-device.c: Better write detection, create iTunes/iTunesDB
	structure if not exist. Confirm everything works with a fresh/reset/empty
	iPod.

2005-08-22  Aaron Bockover  <aaron@aaronbock.net>

	* libipoddevice: Version 0.2.7
	* configure.ac: Auto-detect submount
	* libipoddevice: Version 0.2.6
	* src/tests: Removed
	* src/ipod-device.c,src/ipod-device.h: ipod_device_detect_writeable: 
	detect if write support is possible on device (hfs+ can't usually 
	write, may be cases of other formats mounted as RO)

2005-08-15  JP Rosevear  <jpr@novell.com>

	* src/Makefile.am: tidy build

2005-08-08  Aaron Bockover  <aaron@aaronbock.net>

	* src/ipod-device.c: (ipod_device_detect_model) Fix bug that would
	return type as regular if iPod is actually a photo

2005-08-03  Aaron Bockover  <aaron@aaronbock.net>

	* libipoddevice: Version 0.2.5.1
	* src/Makefile.am: -Wall and -D_FORTIFY_SOURCE=2
	* src/ipod-device.c, src/ipod-device-event-listener.c,
	src/ipod-device-test.c: gcc patches for warnings, thanks gekker!
	* libipoddevice: Version 0.2.5
	* src/ipod-device.c: Small bug fix
	* src/Makefile.am: Fixed build system to distcheck for gekker

2005-07-26  Aaron Bockover  <aaron@aaronbock.net>

	* src/ipod-device.c: (ipod_device_read_sysinfo): Fixed double free
	* src/ipod-device.c: Two HAL volume properties are exposed: volume-uuid
	and volume-label.
	* src/ipod-device.c: (ipod_device_read_sysinfo): reads the SysInfo Device
	file for the serial number, model number, and firmware version, which
	populates three new properties: serial-number, model-number, 
	firmware-version
	* src/ipod-device.c: ipod_device_save() now creates the iPod_Control/iTunes
	directory if it doesn't exist
	* src/ipod-device.c: New property, is-new, set to TRUE when iPod is
	fresh from the factory or a restore, (missing 
	iPod_Control/iTunes/DeviceInfo)
	* src/ipod-device-event-listener.c: Syntax cleanup
	* src/ipod-device-test.c: New option, --set-properties to set the
	device, user, and host name of the iPod

2005-07-18  Aaron Bockover  <aaron@aaronbock.net>

	* configure.ac: Version 0.2.4
	* src/ipod-device-event-listener.c: object init uses 
	ipod_device_list_device_udis to get initial list of potential iPods
	* src/test/Makefile: same change here regarding binary name
	* src/Makefile.am: ipod-device-test binary is now just `ipod'
	* src/ipod-device-test.c: new options: --list-udis to list device 
	UDIs, and --eject now does not require a device ID - if no device ID
	is passed, it ejects all iPods
	* src/ipod-device.c: (ipod_device_list_device_udis): new function to
	list just the volume IDs of potential iPod devices, much faster than
	ipod_device_list_devices since no actual device is created, and no
	disk interaction is needed
	
	* configure.ac: Default to pumount, fall back on umount
	* libipoddevice: Version 0.2.3
	* src/ipod-device.c: (ipod_device_dir_size) fixed memory issue with
	guint64 under gcc-4 while calculating directory size recursively.

2005-07-12  James Willcox  <james@ximian.com>

	* src/ipod-device-event-listener.c: (g_list_find_string_index),
	(ipod_device_event_listener_init), (add_device), (remove_device),
	(hal_device_added), (hal_device_removed),
	(hal_device_property_modified),
	(ipod_device_event_listener_hal_init),
	(ipod_device_event_listener_udi_is_ipod):

	Receive property change events, to detect when an ipod has been
	mounted or unmounted.  Also fix some memory corruption when removing
	a device, and get an initial list of devices in the instance init.

	* src/ipod-device-event-listener.h: Make the 'udi' argument for the
	signal callbacks const.

2005-07-12 Aaron Bockover <aaron@aaronbock.net>

	* src/Makefile.am: Added rule to build and install test
	* src/ipod-device-test.c: Much better tests, actual usable program
	* src/ipod-device-test.c: Moved from test
	* src/ipod-device.c: hd_hal_id==NULL check
	* src/ipod-device.c: Removed duplicate HAL context 

2005-07-11 Aaron Bockover <aaron@aaronbock.net>

	* src/hal-common.[ch]: Added back, how funny, now with 
	ipod_device_set_global_main_context
	* src/ipod-device-event-listener.[ch]: Got rid of the aforementioned 
	GObject property, using only a global one for both objects

	* ipod-device: Version 0.2.1
	* src/ipod-device-event-listener.h: signatures for new constructor/function
	* src/ipod-device-event-listener.c: new GObject property, main-context
	for setting the GMainContext for the D-Bus connection, new constructor,
	ipod_device_event_listener_g_main_context_new, and a global set function
	for a global GMainContext, 
	ipod_device_event_listener_set_global_g_main_context
	* src/hal-common.c, src/hal-common.h: removed
	* src/ipod-device.c: moved ipod_device_hal_initialize from hal-common.c
	* src/ipod-device-event-listener.c: moved hal_mainloop_integration from
	hal-common.c

2005-07-11 James Willcox <snorp@snorp.net>

	* src/Makefile.am: install ipod-device-event-listener.h
	* src/hal-common.c: (hal_mainloop_integration): uncomment, the g_main
	hookup stuff, as we need that.
	* src/ipod-device-event-listener.h: remove test_signals as there is no
	function for that.
	* src/test/ipod-device-test.c: (device_added_cb), (device_removed_cb), 
	(main): hook up to add/remove signals.

2005-07-10 James Willcox <snorp@snorp.net>

	* configure.ac: link with dbus-glib-1
	* src/ipod-device.c: (ipod_device_write_device_info_string): if the
	string is NULL, write an empty one.

2005-07-05 Aaron Bockover <aaron@aaronbock.net>

	* ipod-device: third tarball release (0.2.0)
	* ipod-device-event-listener.c: New Object, "IpodDeviceEventListener" 
	allow programs to subscribe to iPod/HAL specific events
	* ipod-device.c: Eject code now explicitly calls an unmount program before 
	eject, except if submount is used. Supports custom EJECT_COMMAND and 
	UNMOUNT_COMMAND paths, and also ASSUME_SUBMOUNT can be set at 
	configure time (concept taken from g-v-m)
	* ipod-device.c: Ported HAL code to new 0.5 version of the API

2005-06-17 Aaron Bockover <aaron@aaronbock.net>

	* ipod-device.c: Fixed eject command
	* ipod-device.c: Added detection of iPod Mini
	* ipod-device.c: GError reporting on some functions
	* ipod-device.c: sync() patch from snorp

2005-06-16 Aaron Bockover <aaron@aaronbock.net>

	* ipod-device: updated build system

	* ipod-device.c: Added file descriptor checking on mount path before
	  trying to eject device. Normally eject would fail if a device had
	  an open file descriptor, but it succeeds in the case where subfs
	  has mounted the device. A bug has been filed with submount, but
	  the code exists here just in case.

2005-06-15 Aaron Bockover <aaron@aaronbock.net>

	* ipod-device.c: Fixed volume support for non-ipod image paths
	* ipod-device.c: Added volume support for non-ipod image paths

2005-06-14 Aaron Bockover <aaron@aaronbock.net>

	* ipod-device: second tarball release (0.1.1)
	* ipod-device: Assigned Copyright to Novell
	* ipod-device: first tarball release (0.1)
	* ipod-device.c: ipod_device_save() now writes DeviceInfo file
	* ipod-device.c: device-name, user-name, host-name properties now
	read/write
	* ipod-device.c: ipod_device_rescan_disk() recalculates disk usage

2005-06-13 Aaron Bockover <aaron@aaronbock.net>

	* ipod-device.c: Added directory scanning for sizing, now have the
	"volume-used" and "volume-available" properties working.

2005-06-12 Aaron Bockover <aaron@aaronbock.net>

	* ipod-device: 0.1 First Release
	* ipod-device: autobuild system for library
	* ipod-device: First ChangeLog entries

